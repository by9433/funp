function run_funp_FC_static(preproc_data_total_list, atlas_path, atlas_name, atlas_ext, standard_path, standard_name, standard_ext)
clc

beta = 6;  % scale-free index

disp('%%%%%%%%%%%%%%% Settings %%%%%%%%%%%%%%%')
num_subj = size(preproc_data_total_list,1);
disp(strcat(['### Number of subjects: ',int2str(num_subj)]));

atlas = strcat(atlas_path,atlas_name,atlas_ext);
disp(strcat(['    >> Atlas: ',atlas]));

if isempty(standard_path) == 1
    disp(strcat(['    >> Preprocessed data and atals are in the same dimension']));
else
    template = strcat(standard_path,standard_name,standard_ext);
    disp(strcat(['    >> Standard: ',template]));
end
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')

if isempty(standard_path) ~= 0
    disp(' ');
    disp('%%%%% Prepare atlas');
    
    if exist('/tmp/tempAtlas')
        system(['rm -rf /tmp/tempAtlas']);
    end
    
    atlas = strcat(atlas_path,atlas_name,atlas_ext);
    mkdir('/tmp/tempAtlas');
    in_atlas = load_nii(atlas);
    atlas_idx = unique(in_atlas.img(:));
    if atlas_idx(1) == 0
        atlas_idx(1,:) = [];
    end
    NumROI = size(atlas_idx,1);
    for nr = 1:NumROI
        mask_idx = find(in_atlas.img(:) == atlas_idx(nr));
        temp_img = zeros(size(in_atlas.img));
        temp_img(mask_idx) = 1;
        temp_nii = in_atlas;
        temp_nii.img = temp_img;
        
        if nr < 10
            temp_nr = strcat('00',int2str(nr));
        elseif nr < 100
            temp_nr = strcat('0',int2str(nr));
        elseif nr < 1000
            temp_nr = int2str(nr);
        end
        save_nii(temp_nii, strcat('/tmp/tempAtlas/atlas_',temp_nr,'.nii'));
        
        system(strcat([ '3dresample -orient RPI ',...
            ' -prefix /tmp/tempAtlas/RPI_atlas_',temp_nr,'.nii ',...
            ' -inset /tmp/tempAtlas/atlas_',temp_nr,'.nii']));
    end
end

for list = 1:num_subj
    if isunix == 1
        temp = strsplit(preproc_data_total_list(list,:),'/');
    elseif ismac == 1
        temp = strsplit(preproc_data_total_list(list,:),'/');
    elseif ispc == 1
        temp = strsplit(preproc_data_total_list(list,:),'\');
    end
    conn_path = [];
    for i = 1:length(temp)-1
        conn_path = strcat([conn_path,temp{i},'/']);
    end
    temp2 = strsplit(temp{end},'.');
    preproc_data_name = temp2{1};
    if length(temp2) > 2
        preproc_data_ext = strcat('.',temp2{2},'.',temp2{3});
    elseif length(temp2) == 2
        preproc_data_ext = strcat('.',temp2{2});
    end
    path_conn_results = strcat(conn_path,preproc_data_name,'_StaticConn_results/');
    mkdir(path_conn_results);
    cd(path_conn_results);
    
    disp(' ');
    disp(strcat(['%%%%% Start processing: ',preproc_data_name,' %%%%%']));
    
    disp(' ');  disp(' ');
    disp('%%%%% Prepare data');
    system(strcat(['3dcopy ',conn_path,preproc_data_name,preproc_data_ext,' ',path_conn_results,preproc_data_name,'.nii.gz']));
    disp(' ');
    
    disp('%%%%% Deoblique Data');
    system(strcat(['3drefit -deoblique ',path_conn_results,preproc_data_name,'.nii.gz']));
    
    disp(' ');
    disp(strcat(['%%%%% Reorient to RPI']));
    system(strcat([ '3dresample ',...
        ' -orient RPI ',...
        ' -prefix ',path_conn_results,'RPI_',preproc_data_name,'.nii.gz ',...
        ' -inset ',path_conn_results,preproc_data_name,'.nii.gz']));
    disp(' ');
    
    
    if isempty(standard_path) == 0
        disp('%%%%% Check data');
        system(strcat(['fslinfo ',template,' | grep ''dim'' > ',path_conn_results,'template.1D']));
        system(strcat(['fslinfo ',atlas,' | grep ''dim'' > ',path_conn_results,'atlas.1D']));
        template_info = importdata(strcat(path_conn_results,'template.1D'));
        atlas_info = importdata(strcat(path_conn_results,'atlas.1D'));
        system(strcat(['rm -rf ',path_conn_results,'template.1D']));
        system(strcat(['rm -rf ',path_conn_results,'atlas.1D']));
        temp = template_info.data;          template_info.data = temp(find(isnan(temp) == 0));
        temp = atlas_info.data;          atlas_info.data = temp(find(isnan(temp) == 0));
        if sum(template_info.data(1:3) - atlas_info.data(1:3)) ~= 0
            error('>> Structural data and atlas are in different space');
        else
            disp('      - Good');
        end
        disp(' ');
        
        disp('%%%%% Register preprocessed data and atlas');
        system(strcat(['fslmaths ',path_conn_results,'RPI_',preproc_data_name,...
            ' -Tmean ',path_conn_results,'Tmean_',preproc_data_name]));
        system(strcat(['flirt -in ',template,...
            ' -ref ',path_conn_results,'Tmean_',preproc_data_name,...
            ' -out ',path_conn_results,'regis_Temp2Data ',...
            ' -omat ',path_conn_results,'regis_Temp2Data.mat ',...
            ' -dof 12']));
        system(strcat(['flirt -applyxfm -init ',path_conn_results,'regis_Temp2Data.mat ',...
            ' -in ',atlas,...
            ' -ref ',path_conn_results,'Tmean_',preproc_data_name,...
            ' -out ',path_conn_results,'regis_Atlas2data.nii.gz',...
            ' -interp nearestneighbour']));
        
        atlas = strcat(path_conn_results,'regis_Atlas2data.nii.gz');
        system(strcat(['fslchfiletype NIFTI ',atlas]));
        atlas = strcat(path_conn_results,'regis_Atlas2data.nii');
        if exist(atlas) == 0
            gunzip(strcat(atlas,'.gz'));
            system(strcat(['rm -rf ',atlas,'.gz']));
        end
        
        
        disp('      - Prepare atlas');
        if exist('/tmp/tempAtlas')
            system(['rm -rf /tmp/tempAtlas']);
        end

        mkdir('/tmp/tempAtlas');
        in_atlas = load_nii(atlas);
        atlas_idx = unique(in_atlas.img(:));
        if atlas_idx(1) == 0
            atlas_idx(1,:) = [];
        end
        NumROI = size(atlas_idx,1);
        for nr = 1:NumROI
            mask_idx = find(in_atlas.img(:) == atlas_idx(nr));
            temp_img = zeros(size(in_atlas.img));
            temp_img(mask_idx) = 1;
            temp_nii = in_atlas;
            temp_nii.img = temp_img;
            
            if nr < 10
                temp_nr = strcat('00',int2str(nr));
            elseif nr < 100
                temp_nr = strcat('0',int2str(nr));
            elseif nr < 1000
                temp_nr = int2str(nr);
            end
            save_nii(temp_nii, strcat('/tmp/tempAtlas/atlas_',temp_nr,'.nii'));
            
            system(strcat([ '3dresample -orient RPI ',...
                ' -prefix /tmp/tempAtlas/RPI_atlas_',temp_nr,'.nii ',...
                ' -inset /tmp/tempAtlas/atlas_',temp_nr,'.nii']));
        end
    else
        disp('%%%%% Check data');
        system(strcat(['fslinfo ',path_conn_results,'RPI_',preproc_data_name,' | grep ''dim'' > ',path_conn_results,'data.1D']));
        system(strcat(['fslinfo ',atlas,' | grep ''dim'' > ',path_conn_results,'atlas.1D']));
        data_info = importdata(strcat(path_conn_results,'data.1D'));
        atlas_info = importdata(strcat(path_conn_results,'atlas.1D'));
        system(strcat(['rm -rf ',path_conn_results,'data.1D']));
        system(strcat(['rm -rf ',path_conn_results,'atlas.1D']));
        temp = data_info.data;          data_info.data = temp(find(isnan(temp) == 0));
        temp = atlas_info.data;          atlas_info.data = temp(find(isnan(temp) == 0));
        if sum(data_info.data(1:3) - atlas_info.data(1:3)) ~= 0
            error('>> Input data and atlas are in different space');
        else
            disp('      - Good');
        end
    end
    
    
    disp(' ');
    disp('%%%%% Extract time series for each ROI');
    mkdir(strcat(path_conn_results,'TimeSeries'));
    
    in_data = strcat(path_conn_results,'RPI_',preproc_data_name,'.nii.gz');
    IClist = dir(strcat('/tmp/tempAtlas/RPI_atlas_*.nii'));
    for ilist = 1:length(IClist)
        disp(strcat(['       Atlas = ',int2str(ilist),' -- ',IClist(ilist).name]));
        
        if ilist < 10
            temp_list = strcat('00',int2str(ilist));
        elseif ilist < 100
            temp_list = strcat('0',int2str(ilist));
        elseif ilist < 1000
            temp_list = int2str(ilist);
        end
        
        system(strcat(['fslmeants -i ',in_data,...
            ' -m /tmp/tempAtlas/',IClist(ilist).name,...
            ' -o ',path_conn_results,'TimeSeries/atlas',temp_list,'.txt']));
    end
    
    temp = importdata(strcat(path_conn_results,'TimeSeries/atlas001.txt'));
    sub_ts = zeros(size(temp,1), length(IClist));
    for ilist = 1:length(IClist)
        if ilist < 10
            temp_list = strcat('00',int2str(ilist));
        elseif ilist < 100
            temp_list = strcat('0',int2str(ilist));
        elseif ilist < 1000
            temp_list = int2str(ilist);
        end
        temp = importdata(strcat(path_conn_results,'TimeSeries/atlas',temp_list,'.txt'));
        % demean
        temp=temp-repmat(mean(temp),size(temp,1),1);
        sub_ts(:,ilist) = temp;
    end
    dlmwrite(strcat(path_conn_results,preproc_data_name,'_ts.txt'), sub_ts);
    
    
    disp(' ');
    disp('%%%%% Construct connectivity matrix');
    system(strcat(['fslinfo ',in_data,' | grep ''dim4'' > ',path_conn_results,'File.1D']));
    func_info = importdata(strcat(path_conn_results,'File.1D'));
    temp = func_info.data;
    func_info.data = temp(find(isnan(temp) == 0));
    TR = func_info.data(2);
    
    varnorm = 1;    % controls variance normalisation: 0=none, 1=normalise whole subject stddev, 2=normalise each separate TimeSeries_ManyICs50 from each subject
    rho = 0.01;
    
    grotALL=load(strcat(path_conn_results,preproc_data_name,'_ts.txt'));
    gn=size(grotALL,1);
    ts.NtimepointsPerSubject=gn;
    
    grot=grotALL(1:gn,:);
    if varnorm==1
        grot=grot/std(grot(:)); % normalise whole subject stddev
    elseif varnorm==2
        grot=grot ./ repmat(std(grot),size(grot,1),1); % normalise each separate TimeSeries_ManyICs50 from each subject
    end
    TS=grot;
    
    ts.ts=TS;
    ts.tr=TR;
    ts.Nsubjects=1;
    ts.Nnodes=size(TS,2);
    ts.NnodesOrig=ts.Nnodes;
    ts.Ntimepoints=size(TS,1);
    ts.NtimepointsPerSubject=ts.NtimepointsPerSubject;
    ts.DD=1:ts.Nnodes;
    ts.UNK=[];
    
    N=ts.Nnodes;
    just_diag=0;
    
    grot=ts.ts(1:ts.NtimepointsPerSubject,:);
    grot=corr(grot); grot(eye(N)>0)=0;
    
    if just_diag==0
        netmats1(1,:)=reshape(grot,1,N*N);
    else
        netmats1(1,:)=grot;
    end
    
    % Full correlation + Ridge regression with regression
    grot=ts.ts(1:ts.NtimepointsPerSubject,:);
    grot=cov(grot);  grot=grot/sqrt(mean(diag(grot).^2));
    grot=-inv(grot+rho*eye(N));
    grot=(grot ./ repmat(sqrt(abs(diag(grot))),1,N)) ./ repmat(sqrt(abs(diag(grot)))',N,1);  grot(eye(N)>0)=0;
    
    if just_diag==0
        netmats2(1,:)=reshape(grot,1,N*N);
    else
        netmats2(1,:)=grot;
    end
    
    % Make correlation matrix
    L = reshape(netmats1,N,N);
    H = reshape(netmats2,N,N);
    ConnMatR = tril(L) + triu(H);
    save(strcat(path_conn_results,'ConnMatR.mat'), 'ConnMatR');
    
    
    disp(' ');
    disp('%%%%% Calculate network parameters');
    % Construct real network
    % Full correlation: tril / Partial correlation: triu
    RealNet_full = tril(ConnMatR);             RealNet_full = RealNet_full + RealNet_full';
    RealNet_partial = triu(ConnMatR);       RealNet_partial = RealNet_partial + RealNet_partial';
    
    RealNet_wei_full = ((RealNet_full+1)/2).^beta;
    RealNet_weiZ_full = .5*log((1+RealNet_wei_full)./(1-RealNet_wei_full));
    RealNet_wei_partial = ((RealNet_partial+1)/2).^beta;
    RealNet_weiZ_partial = .5*log((1+RealNet_wei_partial)./(1-RealNet_wei_partial));
    
    save(strcat(path_conn_results,'RealNet_weiZ_full.mat'), 'RealNet_weiZ_full');
    save(strcat(path_conn_results,'RealNet_weiZ_partial.mat'), 'RealNet_weiZ_partial');
    
    %%%%% Centrality features
    wrcm = RealNet_weiZ_full;
    [B, D, E] = NetworkCentrality(wrcm);
    Hubness_full = B(:,2)'; % hubness
    BC_full = B(:,1)'; % BC
    DC_full = D';      % DC
    EVC_full = E';      % EVC
    
    wrcm = RealNet_weiZ_partial;
    [B, D, E] = NetworkCentrality(wrcm);
    Hubness_partial = B(:,2)'; % hubness
    BC_partial = B(:,1)'; % BC
    DC_partial = D';      % DC
    EVC_partial = E';      % EVC
    
    %%%%% Efficiency
    Eglob_full = efficiency_wei(RealNet_weiZ_full, 0);
    Eloc_full = efficiency_wei(RealNet_weiZ_full, 1)';
    Eglob_partial = efficiency_wei(RealNet_weiZ_partial, 0);
    Eloc_partial = efficiency_wei(RealNet_weiZ_partial, 1)';
    
    %%%%% Module related features
    wrcm = RealNet_weiZ_full;
    [Ci_full, Q_full] = modularity_louvain_und(wrcm);           % Ci: module indices, Q: Modularity
    WMDZ_full = module_degree_zscore(wrcm, Ci_full, 0)'; % within module degree z score
    PC_full = participation_coef(wrcm, Ci_full)';                     % participation coefficient
    
    wrcm = RealNet_weiZ_partial;
    [Ci_partial, Q_partial] = modularity_louvain_und(wrcm);           % Ci: module indices, Q: Modularity
    WMDZ_partial = module_degree_zscore(wrcm, Ci_partial, 0)'; % within module degree z score
    PC_partial = participation_coef(wrcm, Ci_partial)';                     % participation coefficient
    
    save(strcat(path_conn_results,'netparam_full.mat'), 'Hubness_full', 'BC_full', 'DC_full', 'EVC_full', 'Eglob_full', 'Eloc_full', 'Ci_full', 'Q_full', 'WMDZ_full', 'PC_full');
    save(strcat(path_conn_results,'netparam_partial.mat'), 'Hubness_partial', 'BC_partial', 'DC_partial', 'EVC_partial', 'Eglob_partial', 'Eloc_partial', 'Ci_partial', 'Q_partial', 'WMDZ_partial', 'PC_partial');
    
    if isempty(standard_path) == 0
        if exist('/tmp/tempAtlas') ~= 0
            system(strcat(['rm -rf /tmp/tempAtlas']));
        end
    end
end
if exist('/tmp/tempAtlas') ~= 0
    system(strcat(['rm -rf /tmp/tempAtlas']));
end
disp(' ');
disp('%%%%% Static connectivity preprocessing finished %%%%%');
disp(' ');
