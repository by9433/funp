function varargout = funp_FC_static(varargin)
% FUNP_FC_STATIC MATLAB code for funp_FC_static.fig
%      FUNP_FC_STATIC, by itself, creates a new FUNP_FC_STATIC or raises the existing
%      singleton*.
%
%      H = FUNP_FC_STATIC returns the handle to a new FUNP_FC_STATIC or the handle to
%      the existing singleton*.
%
%      FUNP_FC_STATIC('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FUNP_FC_STATIC.M with the given input arguments.
%
%      FUNP_FC_STATIC('Property','Value',...) creates a new FUNP_FC_STATIC or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before funp_FC_static_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to funp_FC_static_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help funp_FC_static

% Last Modified by GUIDE v2.5 19-Nov-2018 00:10:20

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @funp_FC_static_OpeningFcn, ...
                   'gui_OutputFcn',  @funp_FC_static_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before funp_FC_static is made visible.
function funp_FC_static_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to funp_FC_static (see VARARGIN)

% Choose default command line output for funp_FC_static
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes funp_FC_static wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = funp_FC_static_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in load_preproc_data.
function load_preproc_data_Callback(hObject, eventdata, handles)
% hObject    handle to load_preproc_data (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_LoadPreprocData_FC = LoadPreprocData_FC;
handles.h_LoadPreprocData_FC = guihandles(h_LoadPreprocData_FC);
guidata(hObject, handles);


% --- Executes on button press in load_atlas.
function load_atlas_Callback(hObject, eventdata, handles)
% hObject    handle to load_atlas (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
startingFolder = pwd;
cd(startingFolder);
[FileName, folder] = uigetfile('./*');
temp = strsplit(FileName,'.');
handles.atlas_path = folder;
handles.atlas_name = temp{1};
handles.atlas_ext = strcat('.',temp{2});
guidata(hObject, handles);
assignin('base', 'atlas_path', handles.atlas_path);
assignin('base', 'atlas_name', handles.atlas_name);
assignin('base', 'atlas_ext', handles.atlas_ext);


% --- Executes on selection change in YN.
function YN_Callback(hObject, eventdata, handles)
% hObject    handle to YN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns YN contents as cell array
%        contents{get(hObject,'Value')} returns selected item from YN
contents = cellstr(get(hObject,'String'));
YN = contents{get(hObject,'Value')};
handles.YN = YN;
guidata(hObject, handles);

if strcmp(handles.YN, 'NO') == 1
    handles.load_standard.Enable='on';
else
    handles.load_standard.Enable='off';
    handles.standard_path = [];
    handles.standard_name = [];
    handles.standard_ext = [];
    assignin('base', 'standard_path', handles.standard_path);
    assignin('base', 'standard_name', handles.standard_name);
    assignin('base', 'standard_ext', handles.standard_ext);
end


% --- Executes during object creation, after setting all properties.
function YN_CreateFcn(hObject, eventdata, handles)
% hObject    handle to YN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in load_standard.
function load_standard_Callback(hObject, eventdata, handles)
% hObject    handle to load_standard (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
startingFolder = pwd;
cd(startingFolder);
[FileName, folder] = uigetfile('./*');
temp = strsplit(FileName,'.');
handles.standard_path = folder;
handles.standard_name = temp{1};
handles.standard_ext = strcat('.',temp{2});
guidata(hObject, handles);

if strcmp(handles.YN, 'NO') == 1
    fn = fieldnames(handles);
    test = 0;
    for i = 1:size(fn,1)
        if strcmp(fn{i}, 'standard_name') == 1
            test = test + 1;
        end
    end
    if test == 1
        assignin('base', 'standard_path', handles.standard_path);
        assignin('base', 'standard_name', handles.standard_name);
        assignin('base', 'standard_ext', handles.standard_ext);
    else
        error('### You did not select standard data ###')
    end
end


% --- Executes on button press in SAVE.
function SAVE_Callback(hObject, eventdata, handles)
% hObject    handle to SAVE (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
startingFolder = pwd;
cd(startingFolder);
folder = uigetdir('./');
handles.ListName = folder;
guidata(hObject, handles);
save_folder = handles.ListName;

save_funp_FC_static(save_folder, evalin('base','preproc_data_total_list'), ...
    evalin('base','atlas_path'), evalin('base','atlas_name'), evalin('base','atlas_ext'), ...
    evalin('base','standard_path'), evalin('base','standard_name'), evalin('base','standard_ext'));


% --- Executes on button press in GO.
function GO_Callback(hObject, eventdata, handles)
% hObject    handle to GO (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_funp_FC_static(evalin('base','preproc_data_total_list'), ...
    evalin('base','atlas_path'), evalin('base','atlas_name'), evalin('base','atlas_ext'), ...
    evalin('base','standard_path'), evalin('base','standard_name'), evalin('base','standard_ext'));
close(funp_FC_static)


% --- Executes on button press in CANCEL.
function CANCEL_Callback(hObject, eventdata, handles)
% hObject    handle to CANCEL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(funp_FC_static)
