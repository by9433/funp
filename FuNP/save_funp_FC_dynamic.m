function save_funp_FC_dynamic(save_folder, preproc_data_total_list, atlas_path, atlas_name, atlas_ext, standard_path, standard_name, standard_ext, win_size_val)
    save(strcat(save_folder,'/FuNP_FC_dynamic_settings.mat'), 'preproc_data_total_list', 'atlas_path', 'atlas_name', 'atlas_ext', 'standard_path', 'standard_name', 'standard_ext', 'win_size_val');
end