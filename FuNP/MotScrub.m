function varargout = MotScrub(varargin)
% MOTSCRUB MATLAB code for MotScrub.fig
%      MOTSCRUB, by itself, creates a new MOTSCRUB or raises the existing
%      singleton*.
%
%      H = MOTSCRUB returns the handle to a new MOTSCRUB or the handle to
%      the existing singleton*.
%
%      MOTSCRUB('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MOTSCRUB.M with the given input arguments.
%
%      MOTSCRUB('Property','Value',...) creates a new MOTSCRUB or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before MotScrub_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to MotScrub_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help MotScrub

% Last Modified by GUIDE v2.5 08-May-2018 13:13:57

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MotScrub_OpeningFcn, ...
                   'gui_OutputFcn',  @MotScrub_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before MotScrub is made visible.
function MotScrub_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to MotScrub (see VARARGIN)

% Choose default command line output for MotScrub
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes MotScrub wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = MotScrub_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in YN.
function YN_Callback(hObject, eventdata, handles)
% hObject    handle to YN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns YN contents as cell array
%        contents{get(hObject,'Value')} returns selected item from YN
contents = cellstr(get(hObject,'String'));
YN = contents{get(hObject,'Value')};
handles.YN = YN;
guidata(hObject, handles);

if strcmp(handles.YN, 'YES') == 1
    handles.FDthresh.Enable='on';
else 
    handles.FDthresh.Enable='off';
end


% --- Executes during object creation, after setting all properties.
function YN_CreateFcn(hObject, eventdata, handles)
% hObject    handle to YN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function FDthresh_Callback(hObject, eventdata, handles)
% hObject    handle to FDthresh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of FDthresh as text
%        str2double(get(hObject,'String')) returns contents of FDthresh as a double
FDthresh_enterval = str2double(get(hObject,'String'));
handles.FDthresh_enterval = FDthresh_enterval;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function FDthresh_CreateFcn(hObject, eventdata, handles)
% hObject    handle to FDthresh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in OK.
function OK_Callback(hObject, eventdata, handles)
% hObject    handle to OK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ischar(handles.YN) == 1
    motscrub = handles.YN;
    assignin('base', 'motscrub', motscrub);

    if strcmp(handles.YN, 'YES') == 1
        if isnan(handles.FDthresh_enterval) ~= 1
            if handles.FDthresh_enterval >= 0 && handles.FDthresh_enterval <= 1
                assignin('base', 'FDthresh_enterval', handles.FDthresh_enterval);
            else
                error('### Your entered value is out of range ###')
            end
        else
            error('### You did not enter the value or entered non-numeric value ###')
        end
    else
        FDthresh_enterval = [];
        assignin('base', 'FDthresh_enterval', FDthresh_enterval);
    end

else
    error('### You did not select Y/N option ###')
end
if length(motscrub) == 0
    error('### You did not select Y/N option ###')
end

close(MotScrub)


% --- Executes on button press in CANCEL.
function CANCEL_Callback(hObject, eventdata, handles)
% hObject    handle to CANCEL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(MotScrub)
