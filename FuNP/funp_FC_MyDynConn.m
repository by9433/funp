function [OrigMat, RegMat, blambda] = funp_FC_MyDynConn(ts, num_roi, wsize, sigma, num_repetitions)
%% Normalize time series
grot=ts;
grot=grot-repmat(mean(grot),size(grot,1),1); % demean
grot=grot/std(grot(:)); % normalise whole subject stddev
ts = grot;

%% compute_sliding_window
nT = size(ts,1);
if mod(nT, 2) ~= 0
    m = ceil(nT/2);
    x = 0:nT;
else
    m = nT/2;
    x = 0:nT-1;
end

% m = nT/2;
w = round(wsize/2);
% x = 0:nT-1;
gw = exp(- ((x-m).^2)/ (2 * sigma * sigma))';
b = zeros(nT, 1);  b((m -w + 1):(m+w)) = 1;
c = conv(gw, b); c = c/max(c); c = c(m+1:end-m+1);
c = c(1:nT);

%% Dynamic connectivity
numComp = num_roi;
A = repmat(c, 1, numComp);

Nwin = nT - wsize + 1;
%         initial_lambdas = (0.01:.03:3);
initial_lambdas = (0.01:0.01:0.9);

FNCdyn = zeros(Nwin, numComp*(numComp - 1)/2);
Lambdas = zeros(num_repetitions, length(initial_lambdas));

% Apply circular shift to time series
tcwin = zeros(Nwin, nT, numComp);
for ii = 1:Nwin
    Ashift = circshift(A, round(-nT/2) + round(wsize/2) + ii);
    tcwin(ii, :, :) = squeeze(ts).*Ashift;
end

%% L1 regularisation
useMEX = 0;
try
    GraphicalLassoPath([1, 0; 0, 1], 0.1);
    useMEX = 1;
catch
end

disp('     ** L1 regularisation');
Pdyn = zeros(Nwin, numComp*(numComp - 1)/2);

fprintf('\t rep ')
% Loop over no of repetitions
for r = 1:num_repetitions
    fprintf('%d, ', r)
    
    % split time windows
    ntrain = 1;
    [Nwin, nT, nC] = size(tcwin);
    rndWin = randperm(Nwin);
    trainTC = tcwin(rndWin(1:ntrain),:,:);
    testTC = tcwin(rndWin(ntrain+1:end),:,:);
    trainTC = reshape(trainTC, ntrain*nT, nC);
    testTC = reshape(testTC, (Nwin-ntrain)*nT, nC);
    
    trainTC = icatb_zscore(trainTC);
    testTC = icatb_zscore(testTC);
    
    % Compute graphical lasso
    if (useMEX == 1)
        [wList, thetaList] = GraphicalLassoPath(trainTC, initial_lambdas);
    else
        tol = 1e-4;
        maxIter = 1e4;
        S = icatb_cov(trainTC);
        thetaList = zeros(size(S, 1), size(S, 2), length(initial_lambdas));
        wList = thetaList;
        
        for nL = 1:size(wList, 3)
            [thetaList(:, :, nL), wList(:, :, nL)] = icatb_graphicalLasso(S, initial_lambdas(nL), maxIter, tol);
        end
        
    end
    
    obs_cov = icatb_cov(testTC);
    
    % cov_likelihood
    nmodels = size(thetaList,3);
    L = zeros(1,nmodels);
    for ii = 1:nmodels
        % log likelihood
        theta_ii = squeeze(thetaList(:,:,ii));
        L(ii) = -log(det(theta_ii)) + trace(theta_ii*obs_cov);
    end
    
    Lambdas(r, :) = L;
end

fprintf('\n')
[mv, minIND] =min(Lambdas, [], 2);
blambda = mean(initial_lambdas(minIND));
fprintf('\t Best Lambda: %0.3f\n', blambda)

% now actually compute the covariance matrix
fprintf('\t Working on estimating covariance matrix for each time window\n')
for ii = 1:Nwin
    % Compute graphical lasso
    if (useMEX == 1)
        [wList, thetaList] = GraphicalLassoPath(icatb_zscore(squeeze(tcwin(ii, :, :))), blambda);
    else
        tol = 1e-4;
        maxIter = 1e4;
        S = icatb_cov(icatb_zscore(squeeze(tcwin(ii, :, :))));
        thetaList = zeros(size(S, 1), size(S, 2), length(blambda));
        wList = thetaList;
        
        for nL = 1:size(wList, 3)
            [thetaList(:, :, nL), wList(:, :, nL)] = icatb_graphicalLasso(S, blambda(nL), maxIter, tol);
        end
        
    end
    
    a = icatb_corrcov(wList);
    a(1:numComp+1:end) = 0;
    
    % mat2vec
    [n,m] = size(a);
    if n ~=m
        error('mat must be square!')
    end
    temp = ones(n);
    % find the indices of the lower triangle of the matrix
    IND = find((temp-triu(temp))>0);
    vec = a(IND);
    FNCdyn(ii, :) = vec;
    
    InvC = -thetaList;
    r = (InvC ./ repmat(sqrt(abs(diag(InvC))), 1, numComp)) ./ repmat(sqrt(abs(diag(InvC)))', numComp, 1);
    r(1:numComp+1:end) = 0;
    % mat2vec
    [n,m] = size(r);
    if n ~=m
        error('mat must be square!')
    end
    temp = ones(n);
    % find the indices of the lower triangle of the matrix
    IND = find((temp-triu(temp))>0);
    vec = r(IND);
    Pdyn(ii, :) = vec;
end

FNCdyn = atanh(FNCdyn);

%% Construct matrices
OrigMat = zeros(numComp, numComp, Nwin);
RegMat = zeros(numComp, numComp, Nwin);
for ii = 1:Nwin,
    n = 1;
    for i = 1:numComp-1,
        j = numComp - 1 - i + 1;
        OrigMat(i,i+1:end,ii) = FNCdyn(ii,n:n+j-1);
        RegMat(i,i+1:end,ii) = Pdyn(ii,n:n+j-1);
        n = n+j;
    end;
    OrigMat(:,:,ii) = OrigMat(:,:,ii) + OrigMat(:,:,ii)';
    RegMat(:,:,ii) = RegMat(:,:,ii) + RegMat(:,:,ii)';
end;

