function OutData = Process_T1_segment(InData)
disp('%%%%% Segmentation');
system(strcat([ 'fast -t 1 -n 3 -H 0.1 -I 4 -l 20.0 ',...
    ' -o ',InData.path_anat_results,'seg_',InData.anat_name,'.nii.gz ',...
    InData.path_anat_results,InData.anat_name,'.nii.gz' ]));
disp(' ');
OutData = strcat(['seg_',InData.anat_name,'.nii.gz']);
