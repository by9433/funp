function varargout = Reorient(varargin)
% REORIENT MATLAB code for Reorient.fig
%      REORIENT, by itself, creates a new REORIENT or raises the existing
%      singleton*.
%
%      H = REORIENT returns the handle to a new REORIENT or the handle to
%      the existing singleton*.
%
%      REORIENT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in REORIENT.M with the given input arguments.
%
%      REORIENT('Property','Value',...) creates a new REORIENT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Reorient_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Reorient_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Reorient

% Last Modified by GUIDE v2.5 05-May-2018 15:04:40

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Reorient_OpeningFcn, ...
                   'gui_OutputFcn',  @Reorient_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Reorient is made visible.
function Reorient_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Reorient (see VARARGIN)

% Choose default command line output for Reorient
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Reorient wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Reorient_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



% --- Executes on selection change in YN.
function YN_Callback(hObject, eventdata, handles)
% hObject    handle to YN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns YN contents as cell array
%        contents{get(hObject,'Value')} returns selected item from YN
contents = cellstr(get(hObject,'String'));
YN = contents{get(hObject,'Value')};
handles.YN = YN;
guidata(hObject, handles);

if strcmp(handles.YN, 'YES') == 1
    handles.RL.Enable='on';
    handles.PA.Enable='on';
    handles.IS.Enable='on';
else 
    handles.RL.Enable='off';
    handles.PA.Enable='off';
    handles.IS.Enable='off';
end


% --- Executes during object creation, after setting all properties.
function YN_CreateFcn(hObject, eventdata, handles)
% hObject    handle to YN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in RL.
function RL_Callback(hObject, eventdata, handles)
% hObject    handle to RL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns RL contents as cell array
%        contents{get(hObject,'Value')} returns selected item from RL
contents = cellstr(get(hObject,'String'));
RL = contents{get(hObject,'Value')};
handles.RL = RL;
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function RL_CreateFcn(hObject, eventdata, handles)
% hObject    handle to RL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in PA.
function PA_Callback(hObject, eventdata, handles)
% hObject    handle to PA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PA contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PA
contents = cellstr(get(hObject,'String'));
PA = contents{get(hObject,'Value')};
handles.PA = PA;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function PA_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in IS.
function IS_Callback(hObject, eventdata, handles)
% hObject    handle to IS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns IS contents as cell array
%        contents{get(hObject,'Value')} returns selected item from IS
contents = cellstr(get(hObject,'String'));
IS = contents{get(hObject,'Value')};
handles.IS = IS;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function IS_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in OK.
function OK_Callback(hObject, eventdata, handles)
% hObject    handle to OK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ischar(handles.YN) == 1
    reorient = handles.YN;
    assignin('base', 'reorient', reorient);

    if strcmp(handles.YN, 'YES') == 1
        if ischar(handles.RL) == 1 && ischar(handles.PA) == 1 && ischar(handles.IS) == 1
            orientation = [handles.RL, handles.PA, handles.IS];
            assignin('base', 'orientation', orientation);
        else
            error('### You did not select one of the orientations ###')
        end
        if length(orientation) ~= 3
            error('### You did not select one of the orientations ###')
        end
    else
        orientation = [];
        assignin('base', 'orientation', orientation);
    end

else
    error('### You did not select Y/N option ###')
end
if length(reorient) == 0
    error('### You did not select Y/N option ###')
end

close(Reorient)


% --- Executes on button press in CANCEL.
function CANCEL_Callback(hObject, eventdata, handles)
% hObject    handle to CANCEL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(Reorient)
