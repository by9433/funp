function varargout = delNvol(varargin)
% DELNVOL MATLAB code for delNvol.fig
%      DELNVOL, by itself, creates a new DELNVOL or raises the existing
%      singleton*.
%
%      H = DELNVOL returns the handle to a new DELNVOL or the handle to
%      the existing singleton*.
%
%      DELNVOL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DELNVOL.M with the given input arguments.
%
%      DELNVOL('Property','Value',...) creates a new DELNVOL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before delNvol_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to delNvol_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help delNvol

% Last Modified by GUIDE v2.5 08-May-2018 12:59:12

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @delNvol_OpeningFcn, ...
                   'gui_OutputFcn',  @delNvol_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before delNvol is made visible.
function delNvol_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to delNvol (see VARARGIN)

% Choose default command line output for delNvol
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes delNvol wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = delNvol_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in YN.
function YN_Callback(hObject, eventdata, handles)
% hObject    handle to YN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns YN contents as cell array
%        contents{get(hObject,'Value')} returns selected item from YN
contents = cellstr(get(hObject,'String'));
YN = contents{get(hObject,'Value')};
handles.YN = YN;
guidata(hObject, handles);

if strcmp(handles.YN, 'YES') == 1
    handles.VolSec.Enable='on';
else 
    handles.VolSec.Enable='off';
end


% --- Executes during object creation, after setting all properties.
function YN_CreateFcn(hObject, eventdata, handles)
% hObject    handle to YN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in VolSec.
function VolSec_Callback(hObject, eventdata, handles)
% hObject    handle to VolSec (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns VolSec contents as cell array
%        contents{get(hObject,'Value')} returns selected item from VolSec
contents = cellstr(get(hObject,'String'));
volsec = contents{get(hObject,'Value')};
handles.volsec = volsec;
guidata(hObject, handles);

if strcmp(handles.volsec, 'volumes') == 1
    handles.EnterVol.Enable='on';
    handles.EnterSec.Enable='off';
elseif  strcmp(handles.volsec, 'seconds') == 1
    handles.EnterVol.Enable='off';
    handles.EnterSec.Enable='on';
else
    handles.EnterVol.Enable='off';
    handles.EnterSec.Enable='off';
end


% --- Executes during object creation, after setting all properties.
function VolSec_CreateFcn(hObject, eventdata, handles)
% hObject    handle to VolSec (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function EnterVol_Callback(hObject, eventdata, handles)
% hObject    handle to EnterVol (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EnterVol as text
%        str2double(get(hObject,'String')) returns contents of EnterVol as a double
VOLorSEC_enterval = str2double(get(hObject,'String'));
handles.VOLorSEC_enterval = VOLorSEC_enterval;
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function EnterVol_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EnterVol (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function EnterSec_Callback(hObject, eventdata, handles)
% hObject    handle to EnterSec (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EnterSec as text
%        str2double(get(hObject,'String')) returns contents of EnterSec as a double
VOLorSEC_enterval = str2double(get(hObject,'String'));
handles.VOLorSEC_enterval = VOLorSEC_enterval;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function EnterSec_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EnterSec (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on button press in OK.
function OK_Callback(hObject, eventdata, handles)
% hObject    handle to OK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ischar(handles.YN) == 1
    delnvol = handles.YN;
    assignin('base', 'delnvol', delnvol);

    if strcmp(handles.YN, 'YES') == 1
        if ischar(handles.volsec) == 1
            VOLorSEC = handles.volsec;
            assignin('base', 'VOLorSEC', VOLorSEC);
            
            if isnan(handles.VOLorSEC_enterval) ~= 1
                assignin('base', 'VOLorSEC_enterval', handles.VOLorSEC_enterval);
            else
                error('### You did not enter the values or entered non-numeric values ###')
            end
        else
            error('### You did not select volumes or seconds menu ###')
        end
    else
        VOLorSEC = [];
        VOLorSEC_enterval = [];
        assignin('base', 'VOLorSEC', VOLorSEC);
        assignin('base', 'VOLorSEC_enterval', VOLorSEC_enterval);
    end

else
    error('### You did not select Y/N option ###')
end
if length(delnvol) == 0
    error('### You did not select Y/N option ###')
end

close(delNvol)



% --- Executes on button press in CANCEL.
function CANCEL_Callback(hObject, eventdata, handles)
% hObject    handle to CANCEL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(delNvol)
