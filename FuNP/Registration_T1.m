function varargout = Registration_T1(varargin)
% REGISTRATION_T1 MATLAB code for Registration_T1.fig
%      REGISTRATION_T1, by itself, creates a new REGISTRATION_T1 or raises the existing
%      singleton*.
%
%      H = REGISTRATION_T1 returns the handle to a new REGISTRATION_T1 or the handle to
%      the existing singleton*.
%
%      REGISTRATION_T1('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in REGISTRATION_T1.M with the given input arguments.
%
%      REGISTRATION_T1('Property','Value',...) creates a new REGISTRATION_T1 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Registration_T1_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Registration_T1_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Registration_T1

% Last Modified by GUIDE v2.5 05-May-2018 15:38:26

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Registration_T1_OpeningFcn, ...
                   'gui_OutputFcn',  @Registration_T1_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Registration_T1 is made visible.
function Registration_T1_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Registration_T1 (see VARARGIN)

% Choose default command line output for Registration_T1
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Registration_T1 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Registration_T1_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



% --- Executes on selection change in YN.
function YN_Callback(hObject, eventdata, handles)
% hObject    handle to YN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns YN contents as cell array
%        contents{get(hObject,'Value')} returns selected item from YN
contents = cellstr(get(hObject,'String'));
YN = contents{get(hObject,'Value')};
handles.YN = YN;
guidata(hObject, handles);

if strcmp(handles.YN, 'YES') == 1
    handles.load_standard.Enable='on';
    handles.DOF.Enable='on';
    handles.CostFunction.Enable='on';
else 
    handles.load_standard.Enable='off';
    handles.DOF.Enable='off';
    handles.CostFunction.Enable='off';
end


% --- Executes during object creation, after setting all properties.
function YN_CreateFcn(hObject, eventdata, handles)
% hObject    handle to YN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on button press in load_standard.
function load_standard_Callback(hObject, eventdata, handles)
% hObject    handle to load_standard (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
startingFolder = pwd;
cd(startingFolder);
[FileName, folder] = uigetfile('./*');
temp = strsplit(FileName,'.');
handles.standard_path = folder;
handles.standard_name = temp{1};
handles.standard_ext = strcat('.',temp{2});
guidata(hObject, handles);


% --- Executes on selection change in DOF.
function DOF_Callback(hObject, eventdata, handles)
% hObject    handle to DOF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns DOF contents as cell array
%        contents{get(hObject,'Value')} returns selected item from DOF
contents = cellstr(get(hObject,'String'));
DOF = contents{get(hObject,'Value')};
handles.DOF = DOF;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function DOF_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DOF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in OK.
function OK_Callback(hObject, eventdata, handles)
% hObject    handle to OK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ischar(handles.YN) == 1
    regis = handles.YN;
    assignin('base', 'regis_T1', regis);
    
    if strcmp(handles.YN, 'YES') == 1
        fn = fieldnames(handles);
        test = 0;
        for i = 1:size(fn,1)
            if strcmp(fn{i}, 'standard_name') == 1
                test = test + 1;
            end
        end
        if test == 1
            if ischar(handles.standard_name) ~= 0
                assignin('base', 'standard_path', handles.standard_path);
                assignin('base', 'standard_name', handles.standard_name);
                assignin('base', 'standard_ext', handles.standard_ext);
            end
        else
            error('### You did not select standard data ###')
        end
        
        if ischar(handles.DOF) == 1
            if strcmp(handles.DOF, 'Rigid')
                dof = 'r';
            elseif strcmp(handles.DOF, 'Rigid + Affine')
                dof = 'a';
            elseif strcmp(handles.DOF, 'Rigid + Affine + Deformable syn')
                dof = 's';
            elseif strcmp(handles.DOF, 'Rigid + Deformable syn')
                dof = 'sr';
            elseif strcmp(handles.DOF, 'Deformable syn only')
                dof = 'so';
            elseif strcmp(handles.DOF, 'Rigid + Affine + Deformable b-spline syn')
                dof = 'b';
            elseif strcmp(handles.DOF, 'Rigid + Deformable b-spline syn')
                dof = 'br';
            elseif strcmp(handles.DOF, 'Deformable b-spline syn only')
                dof = 'bo';
            end
            assignin('base', 'dof', dof);
        else
            error('### You did not select DOF ###')
        end
        if length(dof) == 0
            error('### You did not select DOF ###')
        end
    else
        standard_path = [];
        standard_name = [];
        standard_ext = [];
        dof = [];
        assignin('base', 'standard_path', standard_path);
        assignin('base', 'standard_name', standard_name);
        assignin('base', 'standard_ext', standard_ext);
        assignin('base', 'dof', dof);
    end
    
else
    error('### You did not select Y/N option ###')
end
if length(regis) == 0
    error('### You did not select Y/N option ###')
end

close(Registration_T1)



% --- Executes on button press in CANCEL.
function CANCEL_Callback(hObject, eventdata, handles)
% hObject    handle to CANCEL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(Registration_T1)


% --- Executes when figure1 is resized.
function figure1_SizeChangedFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
