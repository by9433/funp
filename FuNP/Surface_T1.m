function varargout = Surface_T1(varargin)
% SURFACE_T1 MATLAB code for Surface_T1.fig
%      SURFACE_T1, by itself, creates a new SURFACE_T1 or raises the existing
%      singleton*.
%
%      H = SURFACE_T1 returns the handle to a new SURFACE_T1 or the handle to
%      the existing singleton*.
%
%      SURFACE_T1('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SURFACE_T1.M with the given input arguments.
%
%      SURFACE_T1('Property','Value',...) creates a new SURFACE_T1 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Surface_T1_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Surface_T1_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Surface_T1

% Last Modified by GUIDE v2.5 22-Sep-2018 15:02:41

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Surface_T1_OpeningFcn, ...
                   'gui_OutputFcn',  @Surface_T1_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Surface_T1 is made visible.
function Surface_T1_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Surface_T1 (see VARARGIN)

% Choose default command line output for Surface_T1
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Surface_T1 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Surface_T1_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in load_T1.
function load_T1_Callback(hObject, eventdata, handles)
% hObject    handle to load_T1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_LoadT1 = LoadT1;
handles.h_LoadT1 = guihandles(h_LoadT1);
guidata(hObject, handles);


% --- Executes on button press in load_standard.
function load_standard_Callback(hObject, eventdata, handles)
% hObject    handle to load_standard (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
startingFolder = pwd;
cd(startingFolder);
[FileName, folder] = uigetfile('./*');
temp = strsplit(FileName,'.');
handles.standard_path = folder;
handles.standard_name = temp{1};
handles.standard_ext = strcat('.',temp{2});
assignin('base', 'standard_path', handles.standard_path);
assignin('base', 'standard_name', handles.standard_name);
assignin('base', 'standard_ext', handles.standard_ext);
guidata(hObject, handles);

% --- Executes on button press in standard_vox_size.
function standard_vox_size_Callback(hObject, eventdata, handles)
% hObject    handle to standard_vox_size (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_standard_vox_size = standard_vox_size;
handles.h_standard_vox_size = guihandles(h_standard_vox_size);
guidata(hObject, handles);

% --- Executes on button press in GO.
function GO_Callback(hObject, eventdata, handles)
% hObject    handle to GO (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
preproc_surface_T1(evalin('base','T1_total_list'), evalin('base','standard_path'), evalin('base','standard_name'), evalin('base','standard_ext'), evalin('base','standard_mm'));
close(Surface_T1)


% --- Executes on button press in CANCEL.
function CANCEL_Callback(hObject, eventdata, handles)
% hObject    handle to CANCEL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(Surface_T1)


% --- Executes on button press in SAVE.
function SAVE_Callback(hObject, eventdata, handles)
% hObject    handle to SAVE (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
startingFolder = pwd;
cd(startingFolder);
folder = uigetdir('./');
handles.ListName = folder;
guidata(hObject, handles);
save_folder = handles.ListName;

save_surface_T1(save_folder, evalin('base','T1_total_list'), evalin('base','standard_path'), evalin('base','standard_name'), evalin('base','standard_ext'), evalin('base','standard_mm'));
