function varargout = Registration_fMRI(varargin)
% REGISTRATION_FMRI MATLAB code for Registration_fMRI.fig
%      REGISTRATION_FMRI, by itself, creates a new REGISTRATION_FMRI or raises the existing
%      singleton*.
%
%      H = REGISTRATION_FMRI returns the handle to a new REGISTRATION_FMRI or the handle to
%      the existing singleton*.
%
%      REGISTRATION_FMRI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in REGISTRATION_FMRI.M with the given input arguments.
%
%      REGISTRATION_FMRI('Property','Value',...) creates a new REGISTRATION_FMRI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Registration_fMRI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Registration_fMRI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Registration_fMRI

% Last Modified by GUIDE v2.5 16-May-2018 17:03:32

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Registration_fMRI_OpeningFcn, ...
                   'gui_OutputFcn',  @Registration_fMRI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Registration_fMRI is made visible.
function Registration_fMRI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Registration_fMRI (see VARARGIN)

% Choose default command line output for Registration_fMRI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Registration_fMRI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Registration_fMRI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



% --- Executes on selection change in YN.
function YN_Callback(hObject, eventdata, handles)
% hObject    handle to YN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns YN contents as cell array
%        contents{get(hObject,'Value')} returns selected item from YN
contents = cellstr(get(hObject,'String'));
YN = contents{get(hObject,'Value')};
handles.YN = YN;
guidata(hObject, handles);

if strcmp(handles.YN, 'YES') == 1
    handles.load_T1.Enable='on';
    handles.Open.Enable='on';
    handles.Add.Enable='on';
    handles.Removd.Enable='on';
    handles.load_standard.Enable='on';    
    handles.DOF1.Enable='on';
    handles.CostFunction1.Enable='on';
    handles.DOF2.Enable='on';
    handles.CostFunction2.Enable='on';
else 
    handles.load_T1.Enable='off';
    handles.Open.Enable='off';
    handles.Add.Enable='off';
    handles.Removd.Enable='off';
    handles.load_standard.Enable='off';
    handles.DOF1.Enable='off';
    handles.CostFunction1.Enable='off';
    handles.DOF2.Enable='off';
    handles.CostFunction2.Enable='off';
end


% --- Executes during object creation, after setting all properties.
function YN_CreateFcn(hObject, eventdata, handles)
% hObject    handle to YN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in load_T1.
function load_T1_Callback(hObject, eventdata, handles)
% hObject    handle to load_T1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in Open.
function Open_Callback(hObject, eventdata, handles)
% hObject    handle to Open (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
startingFolder = pwd;
cd(startingFolder);
[FileName, folder] = uigetfile('./*', 'MultiSelect' , 'on');
if iscell(FileName) == 1            % multiple data
    total_folder = cell(length(FileName),1);
    total_FileName = cell(length(FileName),1);
    total_ext = cell(length(FileName),1);
    for list = 1:length(FileName)
        temp = strsplit(FileName{list},'.');
        total_folder{list,1} = folder;
        total_FileName{list,1} = temp{1};
        if length(temp) > 2
            total_ext{list,1} = strcat('.',temp{2},'.',temp{3});
        elseif length(temp) == 2
            total_ext{list,1} = strcat('.',temp{2});
        end
    end    
else            % one data
    total_folder = cell(1);
    total_FileName = cell(1);
    total_ext = cell(1);
    temp = strsplit(FileName,'.');
    total_folder = folder;
    total_FileName = temp{1};
    if length(temp) > 2
        total_ext = strcat('.',temp{2},'.',temp{3});
    elseif length(temp) == 2
        total_ext = strcat('.',temp{2});
    end
end
ListName = strcat(folder,total_FileName,total_ext);
handles.ListName = ListName;
guidata(hObject, handles);


% --- Executes on selection change in OpenListbox.
function OpenListbox_Callback(hObject, eventdata, handles)
% hObject    handle to OpenListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns OpenListbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from OpenListbox
OpenList = handles.ListName;
set(handles.OpenListbox,'string',OpenList)
handles.OpenList = OpenList;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function OpenListbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to OpenListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in Add.
function Add_Callback(hObject, eventdata, handles)
% hObject    handle to Add (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
new = get(handles.OpenListbox, 'string');
old = get(handles.TotalListbox, 'string');
if ischar(old) == 1
    add = strvcat(old, new);
    set(handles.TotalListbox, 'String', add);
else
    set(handles.TotalListbox, 'String', new);
end
existingItems = cellstr(get(handles.TotalListbox,'String'));

if(isempty(existingItems))
else
    handles.Remove.Enable='on';
end

handles.total_list = add;
guidata(hObject, handles);


% --- Executes on button press in Remove.
function Remove_Callback(hObject, eventdata, handles)
% hObject    handle to Remove (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
existingItems = cellstr(get(handles.TotalListbox,'String'));
selectedId = get(handles.TotalListbox,'Value');

if(class(existingItems) == 'char')
   upd_list='';
   set(handles.TotalListbox, 'String', upd_list)
else
   % If the returned list is a cell array there are three cases
   n_items=length(existingItems);
   if(selectedId == 1)
      % The first element has been selected
      upd_list={existingItems{2:end}};
   elseif(selectedId == n_items)
      % The last element has been selected
      upd_list={existingItems{1:end-1}};
      % Set the "Value" property to the previous element
      set(handles.TotalListbox, 'Value', selectedId-1)
   else
      % And element in the list has been selected
      upd_list={existingItems{1:selectedId-1} existingItems{selectedId+1:end}};
   end
end
% Update the list
set(handles.TotalListbox, 'String', upd_list)

if(isempty(existingItems))
   handles.Remove.Enable='off';
end


% --- Executes on selection change in TotalListbox.
function TotalListbox_Callback(hObject, eventdata, handles)
% hObject    handle to TotalListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns TotalListbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from TotalListbox


% --- Executes during object creation, after setting all properties.
function TotalListbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TotalListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on selection change in DOF1.
function DOF1_Callback(hObject, eventdata, handles)
% hObject    handle to DOF1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns DOF1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from DOF1
contents = cellstr(get(hObject,'String'));
DOF1 = contents{get(hObject,'Value')};
handles.DOF1 = DOF1;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function DOF1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DOF1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in load_standard.
function load_standard_Callback(hObject, eventdata, handles)
% hObject    handle to load_standard (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
startingFolder = pwd;
cd(startingFolder);
[FileName, folder] = uigetfile('./*');
temp = strsplit(FileName,'.');
handles.standard_path = folder;
handles.standard_name = temp{1};
handles.standard_ext = strcat('.',temp{2});
guidata(hObject, handles);


% --- Executes on selection change in DOF2.
function DOF2_Callback(hObject, eventdata, handles)
% hObject    handle to DOF2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns DOF2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from DOF2
contents = cellstr(get(hObject,'String'));
DOF2 = contents{get(hObject,'Value')};
handles.DOF2 = DOF2;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function DOF2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DOF2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in OK.
function OK_Callback(hObject, eventdata, handles)
% hObject    handle to OK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ischar(handles.YN) == 1
    regis = handles.YN;
    assignin('base', 'regis_fMRI', regis);
    
    if strcmp(handles.YN, 'YES') == 1
        fn = fieldnames(handles);
        test1 = 0;
        test2 = 0;
        for i = 1:size(fn,1)
            if strcmp(fn{i}, 'total_list') == 1
                test1 = test1 + 1;
            end
            if strcmp(fn{i}, 'standard_name') == 1
                test2 = test2 + 1;
            end
        end
        if test1 == 1
            if ischar(handles.total_list) ~= 0
                assignin('base', 'T1_total_list', handles.total_list);
            end
        else
            error('### You did not select T1 data ###')
        end
        if test2 == 1
            if ischar(handles.standard_name) ~= 0
                assignin('base', 'standard_path', handles.standard_path);
                assignin('base', 'standard_name', handles.standard_name);
                assignin('base', 'standard_ext', handles.standard_ext);
            end
        else
            error('### You did not select standard data ###')
        end
        
        if ischar(handles.DOF1) == 1
            if strcmp(handles.DOF1, 'Rigid')
                dof1 = 'r';
            elseif strcmp(handles.DOF1, 'Rigid + Affine')
                dof1 = 'a';
            end
            assignin('base', 'dof1', dof1);
        else
            error('### You did not select DOF1 ###')
        end
        if length(dof1) == 0
            error('### You did not select DOF1 ###')
        end
        
        
        if ischar(handles.DOF2) == 1
            if strcmp(handles.DOF2, 'Rigid')
                dof2 = 'r';
            elseif strcmp(handles.DOF2, 'Rigid + Affine')
                dof2 = 'a';
            elseif strcmp(handles.DOF2, 'Rigid + Affine + Deformable syn')
                dof2 = 's';
            elseif strcmp(handles.DOF2, 'Rigid + Deformable syn')
                dof2 = 'sr';
            elseif strcmp(handles.DOF2, 'Deformable syn only')
                dof2 = 'so';
            elseif strcmp(handles.DOF2, 'Rigid + Affine + Deformable b-spline syn')
                dof2 = 'b';
            elseif strcmp(handles.DOF2, 'Rigid + Deformable b-spline syn')
                dof2 = 'br';
            elseif strcmp(handles.DOF2, 'Deformable b-spline syn only')
                dof2 = 'bo';
            end
            assignin('base', 'dof2', dof2);
        else
            error('### You did not select DOF2 ###')
        end
        if length(dof2) == 0
            error('### You did not select DOF2 ###')
        end
        
    else
        T1_total_list = [];
        T1_path = [];
        T1_name = [];
        T1_ext = [];
        dof1 = [];
        standard_path = [];
        standard_name = [];
        standard_ext = [];
        dof2 = [];
        assignin('base', 'T1_total_list', T1_total_list);
        assignin('base', 'T1_path', T1_path);
        assignin('base', 'T1_name', T1_name);
        assignin('base', 'T1_ext', T1_ext);
        assignin('base', 'dof1', dof1);
        assignin('base', 'standard_path', standard_path);
        assignin('base', 'standard_name', standard_name);
        assignin('base', 'standard_ext', standard_ext);
        assignin('base', 'dof2', dof2);
    end
    
else
    error('### You did not select Y/N option ###')
end
if length(regis) == 0
    error('### You did not select Y/N option ###')
end

close(Registration_fMRI)



% --- Executes on button press in CANCEL.
function CANCEL_Callback(hObject, eventdata, handles)
% hObject    handle to CANCEL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(Registration_fMRI)
