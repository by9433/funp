function preproc_surface_fMRI(wb_folder, fMRI_total_list, reorient, orientation, delnvol, VOLorSEC, VOLorSEC_enterval, motscrub, FDthresh_enterval, motcor, stcor, stcor2, stcor_opt, slice_order_file, discor, reverse_path, reverse_name, reverse_ext, total_readout_time, skullremoval_fMRI, intnorm, regis_fMRI, T1_total_list, dof1, standard_path, standard_name, standard_ext, dof2, nvremove, FIX_dim, FIX_train, tempfilt, filter_kind, lpcutoff, hpcutoff, smoothing, fwhm_val)
clc

FindPath = which('funp');
if isunix == 1
    temp = strsplit(FindPath,'/');
elseif ismac == 1
    temp = strsplit(FindPath,'/');
elseif ispc == 1
    temp = strsplit(FindPath,'\');
end
HeadPath = [];
for i = 1:length(temp)-1
    HeadPath = strcat([HeadPath,temp{i},'/']);
end

num_subj = size(fMRI_total_list,1);
if strcmp(stcor, 'YES') == 1
    if strcmp(stcor_opt, 'Manual') == 1
        if strcmp(stcor2, 'NO') == 1
            if num_subj ~= size(slice_order_file,1)
                error('### Number of fMRI and slice timing files are not equal');
            end
        end
    end
end
num_subj_T1 = size(T1_total_list,1);
if strcmp(regis_fMRI, 'YES') == 1
    if num_subj ~= num_subj_T1
        error('### Number of fMRI and T1 are not equal');
    end
end

disp('%%%%%%%%%%%%%%% Settings %%%%%%%%%%%%%%%')
disp(strcat(['### Number of subjects: ',int2str(num_subj)]));
disp(strcat(['### Re-orientation: ',reorient]));
if strcmp(reorient, 'YES') == 1
    disp(strcat(['    >> Direction: ',orientation]));
end
disp(strcat(['### Delete first N volumes: ',delnvol]));
if strcmp(delnvol, 'YES') == 1
    disp(strcat(['    >> ',int2str(VOLorSEC_enterval),' ', VOLorSEC]));
end
disp(strcat(['### Slice timing correction: ',stcor]));
if strcmp(stcor, 'YES') == 1
    disp(strcat(['    >> Option: ',stcor_opt]));
    disp(strcat(['    >> Slice order: ',slice_order_file]));
end
disp(strcat(['### Distortion correction: ',discor]));
if strcmp(discor, 'YES') == 1
    rev_data = strcat(reverse_path,reverse_name,reverse_ext);
    disp(strcat(['    >> Reverse data: ',rev_data]));
    disp(strcat(['    >> Total readout time: ',num2str(total_readout_time, '%.4f')]));
end
disp(strcat(['### Motion scrubbing: ',motscrub]));
if strcmp(motscrub, 'YES') == 1
    disp(strcat(['    >> FD threshold: ',num2str(FDthresh_enterval)]));
end
disp(strcat(['### Motion correction: ',motcor]));
disp(strcat(['### Skull removal: ',skullremoval_fMRI]));
disp(strcat(['### Intensity normalization: ',intnorm]));
disp(strcat(['### Registration: ',regis_fMRI]));
if strcmp(regis_fMRI, 'YES') == 1
    template = strcat(standard_path,standard_name,standard_ext);
    disp(strcat(['    >> Preprocessed T1']));
    if strcmp(dof1, 'r')
        disp(strcat(['    >> DOF: Rigid']));
    elseif strcmp(dof1, 'a')
        disp(strcat(['    >> DOF: Rigid + Affine']));
    end
    disp(strcat(['    >> Standard: ',template]));
    if strcmp(dof2, 'r')
        disp(strcat(['    >> DOF: Rigid']));
    elseif strcmp(dof2, 'a')
        disp(strcat(['    >> DOF: Rigid + Affine']));
    elseif strcmp(dof2, 's')
        disp(strcat(['    >> DOF: Rigid + Affine + Deformable syn']));
    elseif strcmp(dof2, 'sr')
        disp(strcat(['    >> DOF: Rigid + Deformable syn']));
    elseif strcmp(dof2, 'so')
        disp(strcat(['    >> DOF: Deformable syn only']));
    elseif strcmp(dof2, 'b')
        disp(strcat(['    >> DOF: Rigid + Affine + Deformable b-spline syn']));
    elseif strcmp(dof2, 'br')
        disp(strcat(['    >> DOF: Rigid + Deformable b-spline syn']));
    elseif strcmp(dof2, 'bo')
        disp(strcat(['    >> DOF: Deformable b-spline syn only']));
    end    
end
disp(strcat(['### Nuisance variable removal: ',nvremove]));
if strcmp(nvremove, 'YES') == 1
    disp(strcat(['    >> Fix ICA dimension: ',int2str(FIX_dim)]));
    disp(strcat(['    >> Fix training data: ',FIX_train]));
end
disp(strcat(['### Temporal filtering: ',tempfilt]));
if strcmp(tempfilt, 'YES') == 1
    cutoff = strcat([num2str(hpcutoff,'%.4f'),' ~ ', num2str(lpcutoff,'%.4f')]);
    disp(strcat(['    >> Filter type: ',filter_kind]));
    disp(strcat(['    >> Frequency cutoff: ',cutoff]));
end
disp(strcat(['### Spatial smoothing: ',smoothing]));
if strcmp(tempfilt, 'YES') == 1
    disp(strcat(['    >> FWHM: ',num2str(fwhm_val,'%.2f'),' mm']));
end
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')

process_order = zeros(11,1);
% 1: Re-orientation
% 2: Delete first N volumes
% 3: Slice timing correction
% 4: Distortion correction
% 5: Motion scrubbing
% 6: Motion correction
% 7: Skull removal
% 8: Intensity normalization
% 9: Registration & Nuisance variable removal
% 10: Temporal filtering
% 11: Spatial smoothing
temp_idx = 1;
if strcmp(reorient, 'YES') == 1
    process_order(1,1) = temp_idx;
    temp_idx = temp_idx + 1;
end
if strcmp(delnvol, 'YES') == 1
    process_order(2,1) = temp_idx;
    temp_idx = temp_idx + 1;
end
if strcmp(stcor, 'YES') == 1
    process_order(3,1) = temp_idx;
    temp_idx = temp_idx + 1;
end
if strcmp(discor, 'YES') == 1
    process_order(4,1) = temp_idx;
    temp_idx = temp_idx + 1;
end
if strcmp(motscrub, 'YES') == 1
    process_order(5,1) = temp_idx;
    temp_idx = temp_idx + 1;
end
if strcmp(motcor, 'YES') == 1
    process_order(6,1) = temp_idx;
    temp_idx = temp_idx + 1;
end
if strcmp(skullremoval_fMRI, 'YES') == 1
    process_order(7,1) = temp_idx;
    temp_idx = temp_idx + 1;
end
if strcmp(intnorm, 'YES') == 1
    process_order(8,1) = temp_idx;
    temp_idx = temp_idx + 1;
end
if strcmp(regis_fMRI, 'YES') == 1 && strcmp(nvremove, 'YES') == 1
    process_order(9,1) = temp_idx;
    temp_idx = temp_idx + 1;
elseif strcmp(regis_fMRI, 'NO') == 1 && strcmp(nvremove, 'NO') == 1
else
    error('### Registration and nuisance variable removal should be performed simultaneously ###')
end
if strcmp(tempfilt, 'YES') == 1
    process_order(10,1) = temp_idx;
    temp_idx = temp_idx + 1;
end
if strcmp(smoothing, 'YES') == 1
    process_order(11,1) = temp_idx;
    temp_idx = temp_idx + 1;
end

% Save settings
if isunix == 1
    temp = strsplit(fMRI_total_list(1,:),'/');
elseif ismac == 1
    temp = strsplit(fMRI_total_list(1,:),'/');
elseif ispc == 1
    temp = strsplit(fMRI_total_list(1,:),'\');
end
func_path = [];
for i = 1:length(temp)-1
    func_path = strcat([func_path,temp{i},'/']);
end
cd(func_path);
cd('../');
% save('FuNP_Surface_fMRI_settings.mat', 'wb_folder', 'fMRI_total_list', 'reorient', 'orientation', 'delnvol', 'VOLorSEC', 'VOLorSEC_enterval', 'motscrub', 'FDthresh_enterval', 'motcor', 'stcor', 'stcor2', 'stcor_opt', 'slice_order_file', 'skullremoval_fMRI', 'intnorm', 'regis_fMRI', 'T1_total_list', 'dof1', 'costfunction1', 'standard_path', 'standard_name', 'standard_ext', 'dof2', 'costfunction2',  'nvremove', 'FIX_dim', 'FIX_train', 'tempfilt', 'filter_kind', 'lpcutoff', 'hpcutoff', 'smoothing', 'fwhm_val');

for list = 1:num_subj
    if isunix == 1
        temp = strsplit(fMRI_total_list(list,:),'/');
    elseif ismac == 1
        temp = strsplit(fMRI_total_list(list,:),'/');
    elseif ispc == 1
        temp = strsplit(fMRI_total_list(list,:),'\');
    end
    func_path = [];
    for i = 1:length(temp)-1
        func_path = strcat([func_path,temp{i},'/']);
    end
    temp2 = strsplit(temp{end},'.');
    func_name = temp2{1};
    if length(temp2) > 2
        func_ext = strcat('.',temp2{2},'.',temp2{3});
    elseif length(temp2) == 2
        func_ext = strcat('.',temp2{2});
    end
    
    disp(' ');
    disp('%%%%% fMRI volume preprocessing start %%%%%');
    
    disp(' ');
    disp('%%%%% Prepare Data');
    path_func_results_volume = strcat(wb_folder(list,:),'/MNINonLinear/Results/Volume/');
    mkdir(path_func_results_volume);
    cd(path_func_results_volume);
    disp(strcat(['    >> ',func_path,func_name,func_ext]));
    disp(' ');
    system(strcat(['3dcopy ',func_path,func_name,func_ext,' ',path_func_results_volume,func_name,'.nii.gz']));
    disp(' ');
    
    system(strcat(['fslmaths ',path_func_results_volume,func_name,'.nii.gz ',path_func_results_volume,'Orig',func_name,'.nii.gz -odt float']));
    system(strcat(['fslinfo ',path_func_results_volume,'Orig',func_name,'.nii.gz | grep ''dim4'' > ',path_func_results_volume,'File.1D']));
    
    func_info = importdata('File.1D');
    temp = func_info.data;
    func_info.data = temp(find(isnan(temp) == 0));
    TR = func_info.data(2);
    Vol = func_info.data(1);
    
    
    % Deoblique Data (only change the header information
    system(strcat(['3drefit -deoblique ',path_func_results_volume,func_name,'.nii.gz']));
    disp(' ');
    
    OutData = strcat([func_name,'.nii.gz']);
    for po = 1:max(process_order)
        po_idx = find(process_order == po);
        
        if po_idx == 1
            % re-orient
            InData.input = OutData;
            InData.path_func_results = path_func_results_volume;
            InData.func_name = func_name;
            InData.orientation = orientation;
            OutData = Process_fMRI_reorient(InData);
        elseif po_idx == 2
            % delete first N volumes
            if strcmp(VOLorSEC, 'volumes')
                vd = ceil(VOLorSEC_enterval);
                Fina = Vol - vd;
                delete_time = vd * TR;
            elseif strcmp(VOLorSEC, 'seconds')
                vd = ceil(VOLorSEC_enterval/TR);
                Fina = Vol - vd;
                delete_time = vd * TR;
            end
            InData.input = OutData;
            InData.path_func_results = path_func_results_volume;
            InData.func_name = func_name;
            InData.delete_time = delete_time;
            InData.vd = vd;
            InData.Fina = Fina;
            OutData = Process_fMRI_delNvol(InData);
        elseif po_idx == 3
            % slice timing correction
            InData.input = OutData;
            InData.path_func_results = path_func_results_volume;
            InData.func_name = func_name;
            InData.TR = TR;
            InData.stcor_opt = stcor_opt;
            if strcmp(stcor2, 'NO') == 1
                InData.slice_order_file = slice_order_file(list,:);
            elseif strcmp(stcor2, 'YES') == 1
                InData.slice_order_file = slice_order_file;
            end
            OutData = Process_fMRI_STCor(InData);
        elseif po_idx == 4
            % distortion correction
            InData.input = OutData;
            InData.rev_data = strcat(reverse_path,reverse_name,reverse_ext);
            InData.total_readout_time = total_readout_time;
            OutData = Process_fMRI_DisCor(InData);
        elseif po_idx == 5
            % motion scrubbing
            InData.input = OutData;
            InData.path_func_results = path_func_results_volume;
            InData.func_name = func_name;
            InData.FDthresh = FDthresh_enterval;
            OutData = Process_fMRI_MotScrub(InData);
        elseif po_idx == 6
            % motion correction
            system(strcat(['fslinfo ',path_func_results_volume,OutData, ' | grep ''dim4'' > ',path_func_results_volume,'File.1D']));
            func_info = importdata('File.1D');
            Vol = func_info.data(1);
            SBRef = ceil(Vol/2);
            disp('%%%%% Make SBRef image');
            system(strcat(['fslroi ',path_func_results_volume,OutData,' ',path_func_results_volume,'SBRef_',func_name,'.nii.gz ',path_func_results_volume,int2str(SBRef),' 1']));
            disp(' ');
            
            InData.input = OutData;
            InData.path_func_results = path_func_results_volume;
            InData.func_name = func_name;
            InData.SBRef = SBRef;
            OutData = Process_fMRI_MotCor(InData);
        elseif po_idx == 7
            % skull removal
            InData.input = OutData;
            InData.path_func_results = path_func_results_volume;
            InData.func_name = func_name;
            OutData = Process_fMRI_skullremoval(InData);
        elseif po_idx == 8
            % intensity normalization
            InData.input = OutData;
            InData.path_func_results = path_func_results_volume;
            InData.func_name = func_name;
            InData.TR = TR;
            OutData = Process_fMRI_IntNorm(InData);
        elseif po_idx == 9
            % registration
            % nuisance variable removal
            disp('%%%%% Make Mean image');
            system(strcat(['fslmaths ',path_func_results_volume,OutData,' -Tmean ',path_func_results_volume,'Mean4reg_',func_name,'.nii.gz']));
            % -Tmean: mean across time
            disp(' ');
            
            % prepare T1 data
            preprocT1 = T1_total_list(list,:);
            system(strcat(['fslmaths ',preprocT1,' ',path_func_results_volume,'highres_',func_name,'.nii.gz']));
            % prepare standard data
            system(strcat(['fslmaths ',standard_path, standard_name, standard_ext,' ',path_func_results_volume,'standard_',func_name,'.nii.gz']));
            % prepare fix directory
            system(strcat('which fix > ',path_func_results_volume,'/fixdir'));
            temp = string( importdata(strcat(path_func_results_volume,'/fixdir')) );
            temp2 = strsplit(temp,'/');
            FIX_dir = [];
            for tmp = 2:length(temp2)-1
                FIX_dir = [FIX_dir, strcat('/',temp2{tmp})];
            end
            
            InData.input = OutData;
            InData.path_func_results = path_func_results_volume;
            InData.func_name = func_name;
            InData.highres = strcat(['highres_',func_name,'.nii.gz']);
            InData.standard = strcat(['standard_',func_name,'.nii.gz']);
            InData.Tmean = strcat(['Mean4reg_',func_name,'.nii.gz']);
            InData.dof1 = dof1;
            InData.TR = TR;
            InData.FIX_dim = FIX_dim;
            InData.FIX_train = strcat([FIX_dir,'/training_files/',FIX_train,'.RData']);
            InData.dof2 = dof2;
            OutData = Process_fMRI_Regis_NVremov(InData);
        elseif po_idx == 10
            % temporal filtering
            InData.input = OutData;
            InData.path_func_results = path_func_results_volume;
            InData.func_name = func_name;
            InData.filter_kind = filter_kind;
            InData.lpcutoff = lpcutoff;
            InData.hpcutoff = hpcutoff;
            OutData = Process_fMRI_TempFilt(InData);
        elseif po_idx == 11
            % spatial smoothing
            InData.input = OutData;
            InData.path_func_results = path_func_results_volume;
            InData.func_name = func_name;
            InData.FWHM = fwhm_val;
            Process_fMRI_Smooth(InData);
        end
    end
    
    if isunix == 1
        system(strcat(['chmod 777 -R ',path_func_results_volume]));
    elseif ismac == 1
        system(strcat(['chmod -R 777 ',path_func_results_volume]));
    elseif ispc == 1
        system(strcat(['chmod 777 -R ',path_func_results_volume]));
    end
    disp('%%%%% fMRI volume preprocessing finished %%%%%');
    disp(' ');
    
    
    disp(' ');
    disp('%%%%% fMRI surface preprocessing start %%%%%');
    
    surf_input = strcat(path_func_results_volume,OutData);
    system(strcat(['fslroi ',path_func_results_volume,OutData,' ',path_func_results_volume,'NoVolSmooth_SBRef.nii.gz ',int2str(SBRef),' 1']));
    surf_SBRef_input = strcat(path_func_results_volume,'NoVolSmooth_SBRef.nii.gz');
    
    system(strcat([HeadPath,'Surface_functional.sh ',wb_folder(list,:),' ',surf_input,' ',surf_SBRef_input,' ',num2str(fwhm_val,'%.2f')]));    
    
    if isunix == 1
        system(strcat(['chmod 777 -R ',path_func_results_volume,'/../Surface']));
    elseif ismac == 1
        system(strcat(['chmod -R 777 ',path_func_results_volume,'/../Surface']));
    elseif ispc == 1
        system(strcat(['chmod 777 -R ',path_func_results_volume,'/../Surface']));
    end
    
    disp(' ');
    disp('%%%%% fMRI surface preprocessing finished %%%%%');
end



