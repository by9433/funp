function OutData = Process_fMRI_MotScrub(InData)
disp(strcat(['%%%%% Motion scrubbing: FD > ',num2str(InData.FDthresh,'%.2f')]));
system(strcat([ 'fsl_motion_outliers ',...
    ' -i ',InData.path_func_results,InData.input,...
    ' -o FD_confound -s FD_metric -p FD_metric_plot --fd --thresh=',num2str(InData.FDthresh)]));
metric = importdata('FD_metric');
severe_mot_vol = find(metric > InData.FDthresh);
if isempty(severe_mot_vol) ~= 1
    disp(strcat(['      - Scrub ',int2str(numel(severe_mot_vol)),' volumes with severe head motion']));
    system(strcat(['fslsplit ',InData.input,' temp -t']));
    temp_flist = dir(strcat(InData.path_func_results,'temp*.nii.gz'));
    for rm_list = 1:length(severe_mot_vol)
        system(strcat(['rm -rf ',InData.path_func_results,temp_flist(severe_mot_vol(rm_list)).name]));
    end
    system(strcat(['fslmerge -t ',InData.path_func_results,'MS_',InData.func_name,'.nii.gz ',InData.path_func_results,'temp*.nii.gz']));
    system(strcat(['rm -rf ',InData.path_func_results,'temp*']));
else
    disp(strcat(['      - Not need to scrub volumes']));
    copyfile(strcat(InData.path_func_results,InData.input), strcat(InData.path_func_results,'MS_',InData.func_name,'.nii.gz'));
end
disp(' ');
OutData = strcat(['MS_',InData.func_name,'.nii.gz']);
