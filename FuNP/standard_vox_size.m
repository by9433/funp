function varargout = standard_vox_size(varargin)
% STANDARD_VOX_SIZE MATLAB code for standard_vox_size.fig
%      STANDARD_VOX_SIZE, by itself, creates a new STANDARD_VOX_SIZE or raises the existing
%      singleton*.
%
%      H = STANDARD_VOX_SIZE returns the handle to a new STANDARD_VOX_SIZE or the handle to
%      the existing singleton*.
%
%      STANDARD_VOX_SIZE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in STANDARD_VOX_SIZE.M with the given input arguments.
%
%      STANDARD_VOX_SIZE('Property','Value',...) creates a new STANDARD_VOX_SIZE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before standard_vox_size_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to standard_vox_size_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help standard_vox_size

% Last Modified by GUIDE v2.5 22-Sep-2018 15:04:05

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @standard_vox_size_OpeningFcn, ...
                   'gui_OutputFcn',  @standard_vox_size_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before standard_vox_size is made visible.
function standard_vox_size_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to standard_vox_size (see VARARGIN)

% Choose default command line output for standard_vox_size
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes standard_vox_size wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = standard_vox_size_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function vox_size_Callback(hObject, eventdata, handles)
% hObject    handle to vox_size (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of vox_size as text
%        str2double(get(hObject,'String')) returns contents of vox_size as a double
standard_mm = str2double(get(hObject,'String'));
handles.standard_mm = standard_mm;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function vox_size_CreateFcn(hObject, eventdata, handles)
% hObject    handle to vox_size (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in OK.
function OK_Callback(hObject, eventdata, handles)
% hObject    handle to OK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fn = fieldnames(handles);
test = 0;
for i = 1:size(fn,1)
    if strcmp(fn{i}, 'standard_mm') == 1
        test = test + 1;
    end
end

if test == 1
    if isnan(handles.standard_mm) ~= 1
        assignin('base', 'standard_mm', handles.standard_mm);
    else
        error('### You entered non-numeric value ###');
    end
else
    error('### You did not enter the voxel size of the standard data ###')
end
close(standard_vox_size)

% --- Executes on button press in CANCEL.
function CANCEL_Callback(hObject, eventdata, handles)
% hObject    handle to CANCEL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(standard_vox_size)
