function preproc_volume_T1(T1_total_list, reorient, orientation, mficor, skullremoval_T1, regis_T1, standard_path, standard_name, standard_ext, dof, segmentation)
clc

disp('%%%%%%%%%%%%%%% Settings %%%%%%%%%%%%%%%')
num_subj = size(T1_total_list,1);
disp(strcat(['### Number of subjects: ',int2str(num_subj)]));
disp(strcat(['### Re-orientation: ',reorient]));
if strcmp(reorient, 'YES') == 1
    disp(strcat(['    >> Direction: ',orientation]));
end
disp(strcat(['### Magnetic field inhomogeneity correction: ',mficor]));
disp(strcat(['### Skull removal: ',skullremoval_T1]));
disp(strcat(['### Registration: ',regis_T1]));
if strcmp(regis_T1, 'YES') == 1
    template = strcat(standard_path,standard_name,standard_ext);
    disp(strcat(['    >> Standard: ',template]));
    if strcmp(dof, 'r')
        disp(strcat(['    >> DOF: Rigid']));
    elseif strcmp(dof, 'a')
        disp(strcat(['    >> DOF: Rigid + Affine']));
    elseif strcmp(dof, 's')
        disp(strcat(['    >> DOF: Rigid + Affine + Deformable syn']));
    elseif strcmp(dof, 'sr')
        disp(strcat(['    >> DOF: Rigid + Deformable syn']));
    elseif strcmp(dof, 'so')
        disp(strcat(['    >> DOF: Deformable syn only']));
    elseif strcmp(dof, 'b')
        disp(strcat(['    >> DOF: Rigid + Affine + Deformable b-spline syn']));
    elseif strcmp(dof, 'br')
        disp(strcat(['    >> DOF: Rigid + Deformable b-spline syn']));
    elseif strcmp(dof, 'bo')
        disp(strcat(['    >> DOF: Deformable b-spline syn only']));
    end    
end
disp(strcat(['### Segmentation: ',segmentation]));
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')

process_order = zeros(5,1);
% 1: Re-orientation
% 2: Magnetic field inhomogeneity correction
% 3: Skull removal
% 4: Registration
% 5: Segmentation
temp_idx = 1;
if strcmp(reorient, 'YES') == 1
    process_order(1,1) = temp_idx;
    temp_idx = temp_idx + 1;
end
if strcmp(mficor, 'YES') == 1
    process_order(2,1) = temp_idx;
    temp_idx = temp_idx + 1;
end
if strcmp(skullremoval_T1, 'YES') == 1
    process_order(3,1) = temp_idx;
    temp_idx = temp_idx + 1;
end
if strcmp(regis_T1, 'YES') == 1
    process_order(4,1) = temp_idx;
    temp_idx = temp_idx + 1;
end
if strcmp(segmentation, 'YES') == 1
    process_order(5,1) = temp_idx;
    temp_idx = temp_idx + 1;
end

% Save settings
if isunix == 1
    temp = strsplit(T1_total_list(1,:),'/');
elseif ismac == 1
    temp = strsplit(T1_total_list(1,:),'/');
elseif ispc == 1
    temp = strsplit(T1_total_list(1,:),'\');
end
anat_path = [];
for i = 1:length(temp)-1
    anat_path = strcat([anat_path,temp{i},'/']);
end
cd(anat_path);
cd('../');
% save('FuNP_Volume_T1_settings.mat', 'T1_total_list', 'reorient', 'orientation', 'mficor', 'skullremoval_T1', 'regis_T1', 'standard_path', 'standard_name', 'standard_ext', 'dof', 'segmentation');

for list = 1:num_subj
    if isunix == 1
        temp = strsplit(T1_total_list(list,:),'/');
    elseif ismac == 1
        temp = strsplit(T1_total_list(list,:),'/');
    elseif ispc == 1
        temp = strsplit(T1_total_list(list,:),'\');
    end
    anat_path = [];
    for i = 1:length(temp)-1
        anat_path = strcat([anat_path,temp{i},'/']);
    end
    temp2 = strsplit(temp{end},'.');
    anat_name = temp2{1};
    if length(temp2) > 2
        anat_ext = strcat('.',temp2{2},'.',temp2{3});
    elseif length(temp2) == 2
        anat_ext = strcat('.',temp2{2});
    end
    
    
    disp(' ');
    disp('%%%%% T1 volume preprocessing start %%%%%');
    
    disp(' ');
    disp('%%%%% Prepare Data');
    path_anat_results = strcat(anat_path,'anat_results_',anat_name,'/');
    mkdir(path_anat_results);
    cd(path_anat_results);
    disp(strcat(['    >> ',anat_path,anat_name,anat_ext]));
    disp(' ');
    system(strcat(['3dcopy ',anat_path,anat_name,anat_ext,' ',path_anat_results,anat_name,'.nii.gz']));
    disp(' ');
    
    % Deoblique Data (only change the header information
    system(strcat(['3drefit -deoblique ',path_anat_results,anat_name,'.nii.gz']));
    disp(' ');
    
    OutData = strcat([anat_name,'.nii.gz']);
    for po = 1:max(process_order)
        po_idx = find(process_order == po);
        
        if po_idx == 1
            % re-orient
            InData.input = OutData;
            InData.path_anat_results = path_anat_results;
            InData.anat_name = anat_name;
            InData.orientation = orientation;
            OutData = Process_T1_reorient(InData);
        elseif po_idx == 2
            % magnetic field inmohogeneity correction
            InData.input = OutData;
            InData.path_anat_results = path_anat_results;
            InData.anat_name = anat_name;
            OutData = Process_T1_mficor(InData);
        elseif po_idx == 3
            % skull removal
            InData.input = OutData;
            InData.path_anat_results = path_anat_results;
            InData.anat_name = anat_name;
            OutData = Process_T1_skullremoval(InData);
        elseif po_idx == 4
            % registration
            InData.input = OutData;
            InData.path_anat_results = path_anat_results;
            InData.anat_name = anat_name;
            InData.standard = strcat(standard_path,standard_name,standard_ext);
            InData.dof = dof;
            OutData = Process_T1_regis(InData);
        elseif po_idx == 5
            % segmentation
            InData.input = OutData;
            InData.path_anat_results = path_anat_results;
            InData.anat_name = anat_name;
            OutData = Process_T1_segment(InData);
        end
    end
    
    if isunix == 1
        system(strcat(['chmod 777 -R ',path_anat_results]));
    elseif ismac == 1
        system(strcat(['chmod -R 777 ',path_anat_results]));
    elseif ispc == 1
        system(strcat(['chmod 777 -R ',path_anat_results]));
    end
    disp('%%%%% T1 volume preprocessing finished %%%%%');
    disp(' ');
end

