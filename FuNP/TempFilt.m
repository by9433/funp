function varargout = TempFilt(varargin)
% TEMPFILT MATLAB code for TempFilt.fig
%      TEMPFILT, by itself, creates a new TEMPFILT or raises the existing
%      singleton*.
%
%      H = TEMPFILT returns the handle to a new TEMPFILT or the handle to
%      the existing singleton*.
%
%      TEMPFILT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TEMPFILT.M with the given input arguments.
%
%      TEMPFILT('Property','Value',...) creates a new TEMPFILT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before TempFilt_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to TempFilt_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help TempFilt

% Last Modified by GUIDE v2.5 08-May-2018 15:17:47

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @TempFilt_OpeningFcn, ...
                   'gui_OutputFcn',  @TempFilt_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before TempFilt is made visible.
function TempFilt_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to TempFilt (see VARARGIN)

% Choose default command line output for TempFilt
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes TempFilt wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = TempFilt_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in YN.
function YN_Callback(hObject, eventdata, handles)
% hObject    handle to YN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns YN contents as cell array
%        contents{get(hObject,'Value')} returns selected item from YN
contents = cellstr(get(hObject,'String'));
YN = contents{get(hObject,'Value')};
handles.YN = YN;
guidata(hObject, handles);

if strcmp(handles.YN, 'YES') == 1
    handles.filter.Enable='on';
else
    handles.filter.Enable='off';
end


% --- Executes during object creation, after setting all properties.
function YN_CreateFcn(hObject, eventdata, handles)
% hObject    handle to YN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in filter.
function filter_Callback(hObject, eventdata, handles)
% hObject    handle to filter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns filter contents as cell array
%        contents{get(hObject,'Value')} returns selected item from filter
contents = cellstr(get(hObject,'String'));
filter_kind = contents{get(hObject,'Value')};
handles.filter_kind = filter_kind;
guidata(hObject, handles);

if strcmp(handles.filter_kind, 'Lowpass') == 1
    handles.LPcutoff.Enable='on';
    handles.HPcutoff.Enable='off';
elseif strcmp(handles.filter_kind, 'Highpass') == 1
    handles.LPcutoff.Enable='off';
    handles.HPcutoff.Enable='on';
elseif strcmp(handles.filter_kind, 'Bandpass') == 1
    handles.LPcutoff.Enable='on';
    handles.HPcutoff.Enable='on';
else
    handles.LPcutoff.Enable='off';
    handles.HPcutoff.Enable='off';
end


% --- Executes during object creation, after setting all properties.
function filter_CreateFcn(hObject, eventdata, handles)
% hObject    handle to filter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function HPcutoff_Callback(hObject, eventdata, handles)
% hObject    handle to HPcutoff (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of HPcutoff as text
%        str2double(get(hObject,'String')) returns contents of HPcutoff as a double
hpcutoff = str2double(get(hObject,'String'));
handles.hpcutoff = hpcutoff;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function HPcutoff_CreateFcn(hObject, eventdata, handles)
% hObject    handle to HPcutoff (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function LPcutoff_Callback(hObject, eventdata, handles)
% hObject    handle to LPcutoff (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of LPcutoff as text
%        str2double(get(hObject,'String')) returns contents of LPcutoff as a double
lpcutoff = str2double(get(hObject,'String'));
handles.lpcutoff = lpcutoff;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function LPcutoff_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LPcutoff (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in OK.
function OK_Callback(hObject, eventdata, handles)
% hObject    handle to OK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ischar(handles.YN) == 1
    tempfilt = handles.YN;
    assignin('base', 'tempfilt', tempfilt);
    
    if strcmp(handles.YN, 'YES') == 1
        
        fn = fieldnames(handles);
        test1 = 0;
        for i = 1:size(fn,1)
            if strcmp(fn{i}, 'filter_kind') == 1
                test1 = test1 + 1;
            end
        end
        
        if test1 == 1
            if strcmp(handles.filter_kind, 'Lowpass') == 1
                
                test2 = 0;
                for i = 1:size(fn,1)
                    if strcmp(fn{i}, 'lpcutoff') == 1
                        test2 = test2 + 1;
                    end
                end
                if test2 == 1                    
                    if isnan(handles.lpcutoff) ~= 1
                        filter_kind = 'lowpass';
                        assignin('base', 'filter_kind', filter_kind);
                        assignin('base', 'lpcutoff', handles.lpcutoff);
                        hpcutoff = [];
                        assignin('base', 'hpcutoff', hpcutoff);
                    else
                        error('### You entered non-numeric value ###')
                    end
                else
                    error('### You did not enter the low-pass cutoff value ###')
                end
                
            elseif strcmp(handles.filter_kind, 'Highpass') == 1
                
                test2 = 0;
                for i = 1:size(fn,1)
                    if strcmp(fn{i}, 'hpcutoff') == 1
                        test2 = test2 + 1;
                    end
                end
                if test2 == 1                    
                    if isnan(handles.hpcutoff) ~= 1
                        filter_kind = 'highpass';
                        assignin('base', 'filter_kind', filter_kind);
                        assignin('base', 'hpcutoff', handles.hpcutoff);
                        lpcutoff = [];
                        assignin('base', 'lpcutoff', lpcutoff);
                    else
                        error('### You entered non-numeric value ###')
                    end
                else
                    error('### You did not enter the high-pass cutoff value ###')
                end
                
            elseif strcmp(handles.filter_kind, 'Bandpass') == 1
                
                test2 = 0;
                for i = 1:size(fn,1)
                    if strcmp(fn{i}, 'lpcutoff') == 1
                        test2 = test2 + 1;
                    end
                end
                for i = 1:size(fn,1)
                    if strcmp(fn{i}, 'hpcutoff') == 1
                        test2 = test2 + 1;
                    end
                end
                if test2 == 2                    
                    if isnan(handles.lpcutoff) ~= 1 && isnan(handles.hpcutoff) ~= 1
                        if handles.lpcutoff > handles.hpcutoff
                            filter_kind = 'bandpass';
                            assignin('base', 'filter_kind', filter_kind);
                            assignin('base', 'lpcutoff', handles.lpcutoff);
                            assignin('base', 'hpcutoff', handles.hpcutoff);
                        else
                            error('### Reset the frequency range ###')
                        end
                    else
                        error('### You entered non-numeric value ###')
                    end
                else
                    error('### You did not enter the cutoff values ###')
                end
                
            else                
            end
        else
            error('### You did not select filter ###')
        end
        
    else
        filter_kind = [];
        lpcutoff = [];
        hpcutoff = [];
        assignin('base', 'filter_kind', filter_kind);
        assignin('base', 'lpcutoff', lpcutoff);
        assignin('base', 'hpcutoff', hpcutoff);
    end
else
    error('### You did not select Y/N option ###')
end
if length(tempfilt) == 0
    error('### You did not select Y/N option ###')
end
close(TempFilt)

% --- Executes on button press in CANCEL.
function CANCEL_Callback(hObject, eventdata, handles)
% hObject    handle to CANCEL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(TempFilt)
