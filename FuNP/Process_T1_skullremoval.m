function OutData = Process_T1_skullremoval(InData)
disp('%%%%% Skull removal');
system(strcat([ '3dSkullStrip ',...
    ' -orig_vol -input ',InData.path_anat_results,InData.input,...
    ' -prefix ',InData.path_anat_results,'SS_',InData.anat_name,'.nii.gz' ]));
disp(' ');
OutData = strcat(['SS_',InData.anat_name,'.nii.gz']);
