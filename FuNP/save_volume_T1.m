function save_volume_T1(save_folder, T1_total_list, reorient, orientation, mficor, skullremoval_T1, regis_T1, standard_path, standard_name, standard_ext, dof, segmentation)
    save(strcat(save_folder,'/FuNP_Volume_T1_settings.mat'), 'T1_total_list', 'reorient', 'orientation', 'mficor', 'skullremoval_T1', 'regis_T1', 'standard_path', 'standard_name', 'standard_ext', 'dof', 'segmentation');
end