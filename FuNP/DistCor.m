function varargout = DistCor(varargin)
% DISTCOR MATLAB code for DistCor.fig
%      DISTCOR, by itself, creates a new DISTCOR or raises the existing
%      singleton*.
%
%      H = DISTCOR returns the handle to a new DISTCOR or the handle to
%      the existing singleton*.
%
%      DISTCOR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DISTCOR.M with the given input arguments.
%
%      DISTCOR('Property','Value',...) creates a new DISTCOR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before DistCor_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to DistCor_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help DistCor

% Last Modified by GUIDE v2.5 16-Feb-2019 15:07:08

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @DistCor_OpeningFcn, ...
                   'gui_OutputFcn',  @DistCor_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before DistCor is made visible.
function DistCor_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to DistCor (see VARARGIN)

% Choose default command line output for DistCor
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes DistCor wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = DistCor_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in YN.
function YN_Callback(hObject, eventdata, handles)
% hObject    handle to YN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns YN contents as cell array
%        contents{get(hObject,'Value')} returns selected item from YN
contents = cellstr(get(hObject,'String'));
YN = contents{get(hObject,'Value')};
handles.YN = YN;
guidata(hObject, handles);

if strcmp(handles.YN, 'YES') == 1
    handles.load_reverse.Enable='on';
    handles.TotReadoutTime.Enable='on';
else 
    handles.load_reverse.Enable='off';
    handles.TotReadoutTime.Enable='off';
end


% --- Executes during object creation, after setting all properties.
function YN_CreateFcn(hObject, eventdata, handles)
% hObject    handle to YN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in load_reverse.
function load_reverse_Callback(hObject, eventdata, handles)
% hObject    handle to load_reverse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
startingFolder = pwd;
cd(startingFolder);
[FileName, folder] = uigetfile('./*');
temp = strsplit(FileName,'.');
handles.reverse_path = folder;
handles.reverse_name = temp{1};
handles.reverse_ext = strcat('.',temp{2});
guidata(hObject, handles);



function TotReadoutTime_Callback(hObject, eventdata, handles)
% hObject    handle to TotReadoutTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TotReadoutTime as text
%        str2double(get(hObject,'String')) returns contents of TotReadoutTime as a double
TotReadoutTime = str2double(get(hObject,'String'));
handles.TotReadoutTime = TotReadoutTime;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function TotReadoutTime_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TotReadoutTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in OK.
function OK_Callback(hObject, eventdata, handles)
% hObject    handle to OK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ischar(handles.YN) == 1
    discor = handles.YN;
    assignin('base', 'discor', discor);
    
    if strcmp(handles.YN, 'YES') == 1
        fn = fieldnames(handles);
        test1 = 0;
        for i = 1:size(fn,1)
            if strcmp(fn{i}, 'reverse_name') == 1
                test1 = test1 + 1;
            end
        end
        if test1 == 1            
            test2 = 0;
            for i = 1:size(fn,1)
                if strcmp(fn{i}, 'TotReadoutTime') == 1
                    test2 = test2 + 1;
                end
            end
            if test2 == 1
                if ischar(handles.reverse_name) ~= 0
                    assignin('base', 'reverse_path', handles.reverse_path);
                    assignin('base', 'reverse_name', handles.reverse_name);
                    assignin('base', 'reverse_ext', handles.reverse_ext);
                    assignin('base', 'total_readout_time', handles.TotReadoutTime);
                end
            else
                error('### You did did not enter the total readout time value ###')
            end
        else
            error('### You did not select reverse data ###')
        end
    else
        reverse_path = [];
        reverse_name = [];
        reverse_ext = [];
        total_readout_time = [];
        assignin('base', 'reverse_path', reverse_path);
        assignin('base', 'reverse_name', reverse_name);
        assignin('base', 'reverse_ext', reverse_ext);
        assignin('base', 'total_readout_time', total_readout_time);
    end
    
else
    error('### You did not select Y/N option ###')
end
if length(discor) == 0
    error('### You did not select Y/N option ###')
end
close(DistCor)


% --- Executes on button press in CANCEL.
function CANCEL_Callback(hObject, eventdata, handles)
% hObject    handle to CANCEL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(DistCor)
