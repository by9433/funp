function varargout = LoadPreprocData_FC(varargin)
% LOADPREPROCDATA_FC MATLAB code for LoadPreprocData_FC.fig
%      LOADPREPROCDATA_FC, by itself, creates a new LOADPREPROCDATA_FC or raises the existing
%      singleton*.
%
%      H = LOADPREPROCDATA_FC returns the handle to a new LOADPREPROCDATA_FC or the handle to
%      the existing singleton*.
%
%      LOADPREPROCDATA_FC('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in LOADPREPROCDATA_FC.M with the given input arguments.
%
%      LOADPREPROCDATA_FC('Property','Value',...) creates a new LOADPREPROCDATA_FC or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before LoadPreprocData_FC_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to LoadPreprocData_FC_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help LoadPreprocData_FC

% Last Modified by GUIDE v2.5 17-Nov-2018 00:02:10

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @LoadPreprocData_FC_OpeningFcn, ...
                   'gui_OutputFcn',  @LoadPreprocData_FC_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before LoadPreprocData_FC is made visible.
function LoadPreprocData_FC_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to LoadPreprocData_FC (see VARARGIN)

% Choose default command line output for LoadPreprocData_FC
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes LoadPreprocData_FC wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = LoadPreprocData_FC_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in Open.
function Open_Callback(hObject, eventdata, handles)
% hObject    handle to Open (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
startingFolder = pwd;
cd(startingFolder);
[FileName, folder] = uigetfile('./*', 'MultiSelect' , 'on');
if iscell(FileName) == 1            % multiple data
    total_folder = cell(length(FileName),1);
    total_FileName = cell(length(FileName),1);
    total_ext = cell(length(FileName),1);
    for list = 1:length(FileName)
        temp = strsplit(FileName{list},'.');
        total_folder{list,1} = folder;
        total_FileName{list,1} = temp{1};
        if length(temp) > 2
            total_ext{list,1} = strcat('.',temp{2},'.',temp{3});
        elseif length(temp) == 2
            total_ext{list,1} = strcat('.',temp{2});
        end
    end    
else            % one data
    total_folder = cell(1);
    total_FileName = cell(1);
    total_ext = cell(1);
    temp = strsplit(FileName,'.');
    total_folder = folder;
    total_FileName = temp{1};
    if length(temp) > 2
        total_ext = strcat('.',temp{2},'.',temp{3});
    elseif length(temp) == 2
        total_ext = strcat('.',temp{2});
    end
end
ListName = strcat(folder,total_FileName,total_ext);
handles.ListName = ListName;
guidata(hObject, handles);


% --- Executes on selection change in OpenListbox.
function OpenListbox_Callback(hObject, eventdata, handles)
% hObject    handle to OpenListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns OpenListbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from OpenListbox
OpenList = handles.ListName;
set(handles.OpenListbox,'string',OpenList)
handles.OpenList = OpenList;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function OpenListbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to OpenListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Add.
function Add_Callback(hObject, eventdata, handles)
% hObject    handle to Add (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
new = get(handles.OpenListbox, 'string');
old = get(handles.TotalListbox, 'string');
if ischar(old) == 1
    add = strvcat(old, new);
    set(handles.TotalListbox, 'String', add);
else
    set(handles.TotalListbox, 'String', new);
end
existingItems = cellstr(get(handles.TotalListbox,'String'));

if(isempty(existingItems))
else
    handles.Remove.Enable='on';
end

handles.total_list = add;
guidata(hObject, handles);


% --- Executes on button press in Remove.
function Remove_Callback(hObject, eventdata, handles)
% hObject    handle to Remove (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
existingItems = cellstr(get(handles.TotalListbox,'String'));
selectedId = get(handles.TotalListbox,'Value');

if(class(existingItems) == 'char')
   upd_list='';
   set(handles.TotalListbox, 'String', upd_list)
else
   % If the returned list is a cell array there are three cases
   n_items=length(existingItems);
   if(selectedId == 1)
      % The first element has been selected
      upd_list={existingItems{2:end}};
   elseif(selectedId == n_items)
      % The last element has been selected
      upd_list={existingItems{1:end-1}};
      % Set the "Value" property to the previous element
      set(handles.TotalListbox, 'Value', selectedId-1)
   else
      % And element in the list has been selected
      upd_list={existingItems{1:selectedId-1} existingItems{selectedId+1:end}};
   end
end
% Update the list
set(handles.TotalListbox, 'String', upd_list)

if(isempty(existingItems))
   handles.Remove.Enable='off';
end


% --- Executes on selection change in TotalListbox.
function TotalListbox_Callback(hObject, eventdata, handles)
% hObject    handle to TotalListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns TotalListbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from TotalListbox


% --- Executes during object creation, after setting all properties.
function TotalListbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TotalListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in OK.
function OK_Callback(hObject, eventdata, handles)
% hObject    handle to OK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fn = fieldnames(handles);
test = 0;
for i = 1:size(fn,1)
    if strcmp(fn{i}, 'total_list') == 1
        test = test + 1;
    end
end

if test == 1
    assignin('base', 'preproc_data_total_list', handles.total_list);
else
    error('### You did not select any data ###')
end
close(LoadPreprocData_FC)


% --- Executes on button press in CANCEL.
function CANCEL_Callback(hObject, eventdata, handles)
% hObject    handle to CANCEL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(LoadPreprocData_FC)
