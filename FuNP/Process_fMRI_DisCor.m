function OutData = Process_fMRI_DisCor(InData)
disp('%%%%% Distortion correction');

% Make an acquisiton parameter file
system(strcat(['fslinfo ',InData.rev_data,' | grep ''dim4'' > ',InData.path_func_results,'FileRev.1D']));
func_info = importdata(strcat(InData.path_func_results,'FileRev.1D'));
temp = func_info.data;
func_info.data = temp(find(isnan(temp) == 0));
RevVol = func_info.data(1);
acqparams = zeros(RevVol*2,4);
for rv = 1:RevVol
    acqparams(rv,2) = -1;
    acqparams(rv+RevVol,2) = 1;
    acqparams(rv,4) = InData.total_readout_time;
    acqparams(rv+RevVol,4) = InData.total_readout_time;
end
dlmwrite(strcat(InData.path_func_results,'/acqparams.txt'), acqparams,'delimiter',' ');

% Make a data for topup
system(strcat(['fslroi ',InData.path_func_results, InData.input,' ',InData.path_func_results,'temp_dc_orig 0 ',int2str(RevVol)]));
system(strcat(['fslroi ',InData.rev_data,' ',InData.path_func_results,'temp_dc_rev 0 ',int2str(RevVol)]));
system(strcat(['fslmerge -t ',InData.path_func_results,'dc_origrev_',InData.func_name,' temp_dc_orig temp_dc_rev']));
system(strcat(['rm -rf ',InData.path_func_results,'/temp_dc*']));

% Perform topup
system(strcat(['topup --imain=',InData.path_func_results,'dc_origrev_',InData.func_name,' --datain=',InData.path_func_results,'acqparams.txt --config=b02b0.cnf ',...
    ' --out=',InData.path_func_results,'topup_res_',InData.func_name,...
    ' --fout=',InData.path_func_results,'topup_field_',InData.func_name,...
    ' --iout=',InData.path_func_results,'topup_unwarped_',InData.func_name]));
system(strcat(['applytopup --imain=',InData.path_func_results,InData.input,' --inindex=1 --datain=',InData.path_func_results,'acqparams.txt ',...
    ' --topup=',InData.path_func_results,'topup_res_',InData.func_name,...
    ' --out=',InData.path_func_results,'dc_',InData.func_name,' --method=jac']));

disp(' ');
OutData = strcat(['dc_',InData.func_name,'.nii.gz']);
