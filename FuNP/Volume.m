function varargout = Volume(varargin)
% VOLUME MATLAB code for Volume.fig
%      VOLUME, by itself, creates a new VOLUME or raises the existing
%      singleton*.
%
%      H = VOLUME returns the handle to a new VOLUME or the handle to
%      the existing singleton*.
%
%      VOLUME('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in VOLUME.M with the given input arguments.
%
%      VOLUME('Property','Value',...) creates a new VOLUME or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Volume_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Volume_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Volume

% Last Modified by GUIDE v2.5 11-May-2018 13:40:58

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Volume_OpeningFcn, ...
                   'gui_OutputFcn',  @Volume_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Volume is made visible.
function Volume_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Volume (see VARARGIN)

% Choose default command line output for Volume
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Volume wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Volume_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in T1.
function T1_Callback(hObject, eventdata, handles)
% hObject    handle to T1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_Volume_T1 = Volume_T1;
handles.h_Volume_T1 = guihandles(h_Volume_T1);
guidata(hObject, handles);


% --- Executes on button press in fMRI.
function fMRI_Callback(hObject, eventdata, handles)
% hObject    handle to fMRI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_Volume_fMRI = Volume_fMRI;
handles.h_Volume_fMRI = guihandles(h_Volume_fMRI);
guidata(hObject, handles);


% --- Executes on button press in T1fMRI.
function T1fMRI_Callback(hObject, eventdata, handles)
% hObject    handle to T1fMRI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
