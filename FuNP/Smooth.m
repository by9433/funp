function varargout = Smooth(varargin)
% SMOOTH MATLAB code for Smooth.fig
%      SMOOTH, by itself, creates a new SMOOTH or raises the existing
%      singleton*.
%
%      H = SMOOTH returns the handle to a new SMOOTH or the handle to
%      the existing singleton*.
%
%      SMOOTH('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SMOOTH.M with the given input arguments.
%
%      SMOOTH('Property','Value',...) creates a new SMOOTH or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Smooth_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Smooth_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Smooth

% Last Modified by GUIDE v2.5 08-May-2018 15:47:24

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Smooth_OpeningFcn, ...
                   'gui_OutputFcn',  @Smooth_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Smooth is made visible.
function Smooth_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Smooth (see VARARGIN)

% Choose default command line output for Smooth
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Smooth wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Smooth_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in YN.
function YN_Callback(hObject, eventdata, handles)
% hObject    handle to YN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns YN contents as cell array
%        contents{get(hObject,'Value')} returns selected item from YN
contents = cellstr(get(hObject,'String'));
YN = contents{get(hObject,'Value')};
handles.YN = YN;
guidata(hObject, handles);

if strcmp(handles.YN, 'YES') == 1
    handles.fwhm.Enable='on';
else
    handles.fwhm.Enable='off';
end


% --- Executes during object creation, after setting all properties.
function YN_CreateFcn(hObject, eventdata, handles)
% hObject    handle to YN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function fwhm_Callback(hObject, eventdata, handles)
% hObject    handle to fwhm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of fwhm as text
%        str2double(get(hObject,'String')) returns contents of fwhm as a double
fwhm_val = str2double(get(hObject,'String'));
handles.fwhm_val = fwhm_val;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function fwhm_CreateFcn(hObject, eventdata, handles)
% hObject    handle to fwhm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in OK.
function OK_Callback(hObject, eventdata, handles)
% hObject    handle to OK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ischar(handles.YN) == 1
    smoothing = handles.YN;
    assignin('base', 'smoothing', smoothing);
    
    if strcmp(handles.YN, 'YES') == 1
        
        fn = fieldnames(handles);
        test = 0;
        for i = 1:size(fn,1)
            if strcmp(fn{i}, 'fwhm_val') == 1
                test = test + 1;
            end
        end
        
        if test == 1
            if isnan(handles.fwhm_val) ~= 1
                assignin('base', 'fwhm_val', handles.fwhm_val);
            else
                error('### You entered non-numeric value ###')
            end
        else
            error('### You did not enter the FWHM value ###')
        end
        
    else
        fwhm_val = [];
        assignin('base', 'fwhm_val', fwhm_val);
    end    
else
    error('### You did not select Y/N option ###')
end
if length(smoothing) == 0
    error('### You did not select Y/N option ###')
end
close(Smooth)

% --- Executes on button press in CANCEL.
function CANCEL_Callback(hObject, eventdata, handles)
% hObject    handle to CANCEL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(Smooth)
