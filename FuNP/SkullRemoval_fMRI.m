function varargout = SkullRemoval_fMRI(varargin)
% SKULLREMOVAL_FMRI MATLAB code for SkullRemoval_fMRI.fig
%      SKULLREMOVAL_FMRI, by itself, creates a new SKULLREMOVAL_FMRI or raises the existing
%      singleton*.
%
%      H = SKULLREMOVAL_FMRI returns the handle to a new SKULLREMOVAL_FMRI or the handle to
%      the existing singleton*.
%
%      SKULLREMOVAL_FMRI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SKULLREMOVAL_FMRI.M with the given input arguments.
%
%      SKULLREMOVAL_FMRI('Property','Value',...) creates a new SKULLREMOVAL_FMRI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before SkullRemoval_fMRI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to SkullRemoval_fMRI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help SkullRemoval_fMRI

% Last Modified by GUIDE v2.5 08-May-2018 14:00:51

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SkullRemoval_fMRI_OpeningFcn, ...
                   'gui_OutputFcn',  @SkullRemoval_fMRI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before SkullRemoval_fMRI is made visible.
function SkullRemoval_fMRI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to SkullRemoval_fMRI (see VARARGIN)

% Choose default command line output for SkullRemoval_fMRI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes SkullRemoval_fMRI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = SkullRemoval_fMRI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in YN.
function YN_Callback(hObject, eventdata, handles)
% hObject    handle to YN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns YN contents as cell array
%        contents{get(hObject,'Value')} returns selected item from YN
contents = cellstr(get(hObject,'String'));
YN = contents{get(hObject,'Value')};
handles.YN = YN;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function YN_CreateFcn(hObject, eventdata, handles)
% hObject    handle to YN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in OK.
function OK_Callback(hObject, eventdata, handles)
% hObject    handle to OK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ischar(handles.YN) == 1
    skullremoval = handles.YN;
    assignin('base', 'skullremoval_fMRI', skullremoval);
else
    error('### You did not select any option ###')
end
if length(skullremoval) == 0
    error('### You did not select any option ###')
end
close(SkullRemoval_fMRI)


% --- Executes on button press in CANCEL.
function CANCEL_Callback(hObject, eventdata, handles)
% hObject    handle to CANCEL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(SkullRemoval_fMRI)
