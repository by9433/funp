function varargout = Surface(varargin)
% SURFACE MATLAB code for Surface.fig
%      SURFACE, by itself, creates a new SURFACE or raises the existing
%      singleton*.
%
%      H = SURFACE returns the handle to a new SURFACE or the handle to
%      the existing singleton*.
%
%      SURFACE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SURFACE.M with the given input arguments.
%
%      SURFACE('Property','Value',...) creates a new SURFACE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Surface_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Surface_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Surface

% Last Modified by GUIDE v2.5 11-May-2018 14:49:00

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Surface_OpeningFcn, ...
                   'gui_OutputFcn',  @Surface_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Surface is made visible.
function Surface_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Surface (see VARARGIN)

% Choose default command line output for Surface
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Surface wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Surface_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in T1.
function T1_Callback(hObject, eventdata, handles)
% hObject    handle to T1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_Surface_T1 = Surface_T1;
handles.h_Surface_T1 = guihandles(h_Surface_T1);
guidata(hObject, handles);


% --- Executes on button press in fMRI.
function fMRI_Callback(hObject, eventdata, handles)
% hObject    handle to fMRI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_Surface_fMRI = Surface_fMRI;
handles.h_Surface_fMRI = guihandles(h_Surface_fMRI);
guidata(hObject, handles);
