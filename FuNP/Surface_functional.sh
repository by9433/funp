#!/bin/bash
set -e

Usage() {
    cat <<EOF
Usage: Surface_functional.sh <head folder> <input data> <input data SBRef> <FWHM>

  <head folder>       Path of wb_adjust
  <input data>        Data to be processed (volume preprocessed data)
  <input data SBRef>  SBRef of input data
  <FWHM>              Smoothing parameter
EOF
    exit 1
}

[ "$2" = "" ] && Usage

############################# Settings #############################
HeadFolder="$1"
InData="$2"
InDataSBRef="$3"
FWHM="$4"
####################################################################

LowResMesh="32"
RegName="reg.reg_LR"

#Naming Conventions
AtlasSpaceFolder="$HeadFolder"/MNINonLinear
T1wFolder="$HeadFolder"/T1w
NativeFolder="Native"
ResultsFolder="$AtlasSpaceFolder"/Results
DownSampleFolder="fsaverage_LR${LowResMesh}k"
ROIFolder="$AtlasSpaceFolder"/ROIs

echo -e "\n########## Settings ##########"
echo -e "HeadFolder: ${HeadFolder}"
echo -e "FWHM: ${FWHM}"
echo -e "LowResMeshes: ${LowResMesh}"
echo -e "##############################"




#Surface processing
ResultsFolderSurf="$ResultsFolder"/Surface

#Make fMRI Ribbon
#Noisy Voxel Outlier Exclusion
#Ribbon-based Volume to Surface mapping and resampling to standard surface

echo -e "\n### START: Ribbon Volume To Surface Mapping"
mkdir -p "$ResultsFolderSurf"/RibbonVolumeToSurfaceMapping

WorkingDirectory="$ResultsFolderSurf"/RibbonVolumeToSurfaceMapping 
DownsampleFolder="$AtlasSpaceFolder"/"$DownSampleFolder" 
AtlasSpaceNativeFolder="$AtlasSpaceFolder"/"$NativeFolder" 
RegName="${RegName}"

NeighborhoodSmoothing="5"
Factor="0.5"


LeftGreyRibbonValue="1"
RightGreyRibbonValue="1"

for Hemisphere in L R ; do
  if [ $Hemisphere = "L" ] ; then
    GreyRibbonValue="$LeftGreyRibbonValue"
  elif [ $Hemisphere = "R" ] ; then
    GreyRibbonValue="$RightGreyRibbonValue"
  fi
  echo -e "    - $Hemisphere"

  OMP_NUM_THREADS=2 wb_command -create-signed-distance-volume "$AtlasSpaceNativeFolder"/"$Hemisphere".white.native.surf.gii "$InDataSBRef" "$WorkingDirectory"/"$Hemisphere".white.native.nii.gz
  OMP_NUM_THREADS=2 wb_command -create-signed-distance-volume "$AtlasSpaceNativeFolder"/"$Hemisphere".pial.native.surf.gii "$InDataSBRef" "$WorkingDirectory"/"$Hemisphere".pial.native.nii.gz
  fslmaths "$WorkingDirectory"/"$Hemisphere".white.native.nii.gz -thr 0 -bin -mul 255 "$WorkingDirectory"/"$Hemisphere".white_thr0.native.nii.gz
  fslmaths "$WorkingDirectory"/"$Hemisphere".white_thr0.native.nii.gz -bin "$WorkingDirectory"/"$Hemisphere".white_thr0.native.nii.gz
  fslmaths "$WorkingDirectory"/"$Hemisphere".pial.native.nii.gz -uthr 0 -abs -bin -mul 255 "$WorkingDirectory"/"$Hemisphere".pial_uthr0.native.nii.gz
  fslmaths "$WorkingDirectory"/"$Hemisphere".pial_uthr0.native.nii.gz -bin "$WorkingDirectory"/"$Hemisphere".pial_uthr0.native.nii.gz
  fslmaths "$WorkingDirectory"/"$Hemisphere".pial_uthr0.native.nii.gz -mas "$WorkingDirectory"/"$Hemisphere".white_thr0.native.nii.gz -mul 255 "$WorkingDirectory"/"$Hemisphere".ribbon.nii.gz
  fslmaths "$WorkingDirectory"/"$Hemisphere".ribbon.nii.gz -bin -mul $GreyRibbonValue "$WorkingDirectory"/"$Hemisphere".ribbon.nii.gz
  rm -rf "$WorkingDirectory"/"$Hemisphere".white.native.nii.gz "$WorkingDirectory"/"$Hemisphere".white_thr0.native.nii.gz "$WorkingDirectory"/"$Hemisphere".pial.native.nii.gz "$WorkingDirectory"/"$Hemisphere".pial_uthr0.native.nii.gz
done

fslmaths "$WorkingDirectory"/L.ribbon.nii.gz -add "$WorkingDirectory"/R.ribbon.nii.gz "$WorkingDirectory"/ribbon_only.nii.gz
rm -rf "$WorkingDirectory"/L.ribbon.nii.gz "$WorkingDirectory"/R.ribbon.nii.gz

fslmaths "$InData" -Tmean "$WorkingDirectory"/mean -odt float
fslmaths "$InData" -Tstd "$WorkingDirectory"/std -odt float
fslmaths "$WorkingDirectory"/std -div "$WorkingDirectory"/mean "$WorkingDirectory"/cov

fslmaths "$WorkingDirectory"/cov -mas "$WorkingDirectory"/ribbon_only.nii.gz "$WorkingDirectory"/cov_ribbon

fslmaths "$WorkingDirectory"/cov_ribbon -div `fslstats "$WorkingDirectory"/cov_ribbon -M` "$WorkingDirectory"/cov_ribbon_norm
fslmaths "$WorkingDirectory"/cov_ribbon_norm -bin -s $NeighborhoodSmoothing "$WorkingDirectory"/SmoothNorm
fslmaths "$WorkingDirectory"/cov_ribbon_norm -s $NeighborhoodSmoothing -div "$WorkingDirectory"/SmoothNorm -dilD "$WorkingDirectory"/cov_ribbon_norm_s$NeighborhoodSmoothing
fslmaths "$WorkingDirectory"/cov -div `fslstats "$WorkingDirectory"/cov_ribbon -M` -div "$WorkingDirectory"/cov_ribbon_norm_s$NeighborhoodSmoothing "$WorkingDirectory"/cov_norm_modulate
fslmaths "$WorkingDirectory"/cov_norm_modulate -mas "$WorkingDirectory"/ribbon_only.nii.gz "$WorkingDirectory"/cov_norm_modulate_ribbon

STD=`fslstats "$WorkingDirectory"/cov_norm_modulate_ribbon -S`
MEAN=`fslstats "$WorkingDirectory"/cov_norm_modulate_ribbon -M`
Lower=`echo "$MEAN - ($STD * $Factor)" | bc -l`
Upper=`echo "$MEAN + ($STD * $Factor)" | bc -l`

fslmaths "$WorkingDirectory"/mean -bin "$WorkingDirectory"/mask
fslmaths "$WorkingDirectory"/cov_norm_modulate -thr $Upper -bin -sub "$WorkingDirectory"/mask -mul -1 "$WorkingDirectory"/goodvoxels

for Hemisphere in L R ; do
  echo -e "    - $Hemisphere"
  
  for Map in mean cov ; do
  echo -e "      - $Map"

    OMP_NUM_THREADS=2 wb_command -volume-to-surface-mapping "$WorkingDirectory"/"$Map".nii.gz "$AtlasSpaceNativeFolder"/"$Hemisphere".midthickness.native.surf.gii "$WorkingDirectory"/"$Hemisphere"."$Map".native.func.gii -ribbon-constrained "$AtlasSpaceNativeFolder"/"$Hemisphere".white.native.surf.gii "$AtlasSpaceNativeFolder"/"$Hemisphere".pial.native.surf.gii -volume-roi "$WorkingDirectory"/goodvoxels.nii.gz
    OMP_NUM_THREADS=2 wb_command -metric-dilate "$WorkingDirectory"/"$Hemisphere"."$Map".native.func.gii "$AtlasSpaceNativeFolder"/"$Hemisphere".midthickness.native.surf.gii 10 "$WorkingDirectory"/"$Hemisphere"."$Map".native.func.gii -nearest
    OMP_NUM_THREADS=2 wb_command -metric-mask "$WorkingDirectory"/"$Hemisphere"."$Map".native.func.gii "$AtlasSpaceNativeFolder"/"$Hemisphere".roi.native.shape.gii "$WorkingDirectory"/"$Hemisphere"."$Map".native.func.gii
    OMP_NUM_THREADS=2 wb_command -volume-to-surface-mapping "$WorkingDirectory"/"$Map".nii.gz "$AtlasSpaceNativeFolder"/"$Hemisphere".midthickness.native.surf.gii "$WorkingDirectory"/"$Hemisphere"."$Map"_all.native.func.gii -ribbon-constrained "$AtlasSpaceNativeFolder"/"$Hemisphere".white.native.surf.gii "$AtlasSpaceNativeFolder"/"$Hemisphere".pial.native.surf.gii
    OMP_NUM_THREADS=2 wb_command -metric-mask "$WorkingDirectory"/"$Hemisphere"."$Map"_all.native.func.gii "$AtlasSpaceNativeFolder"/"$Hemisphere".roi.native.shape.gii "$WorkingDirectory"/"$Hemisphere"."$Map"_all.native.func.gii
    OMP_NUM_THREADS=2 wb_command -metric-resample "$WorkingDirectory"/"$Hemisphere"."$Map".native.func.gii "$AtlasSpaceNativeFolder"/"$Hemisphere".sphere.${RegName}.native.surf.gii "$DownsampleFolder"/"$Hemisphere".sphere."$LowResMesh"k_fs_LR.surf.gii ADAP_BARY_AREA "$WorkingDirectory"/"$Hemisphere"."$Map"."$LowResMesh"k_fs_LR.func.gii -area-surfs "$AtlasSpaceNativeFolder"/"$Hemisphere".midthickness.native.surf.gii "$DownsampleFolder"/"$Hemisphere".midthickness."$LowResMesh"k_fs_LR.surf.gii -current-roi "$AtlasSpaceNativeFolder"/"$Hemisphere".roi.native.shape.gii
    OMP_NUM_THREADS=2 wb_command -metric-mask "$WorkingDirectory"/"$Hemisphere"."$Map"."$LowResMesh"k_fs_LR.func.gii "$DownsampleFolder"/"$Hemisphere".atlasroi."$LowResMesh"k_fs_LR.shape.gii "$WorkingDirectory"/"$Hemisphere"."$Map"."$LowResMesh"k_fs_LR.func.gii
    OMP_NUM_THREADS=2 wb_command -metric-resample "$WorkingDirectory"/"$Hemisphere"."$Map"_all.native.func.gii "$AtlasSpaceNativeFolder"/"$Hemisphere".sphere.${RegName}.native.surf.gii "$DownsampleFolder"/"$Hemisphere".sphere."$LowResMesh"k_fs_LR.surf.gii ADAP_BARY_AREA "$WorkingDirectory"/"$Hemisphere"."$Map"_all."$LowResMesh"k_fs_LR.func.gii -area-surfs "$AtlasSpaceNativeFolder"/"$Hemisphere".midthickness.native.surf.gii "$DownsampleFolder"/"$Hemisphere".midthickness."$LowResMesh"k_fs_LR.surf.gii -current-roi "$AtlasSpaceNativeFolder"/"$Hemisphere".roi.native.shape.gii
    OMP_NUM_THREADS=2 wb_command -metric-mask "$WorkingDirectory"/"$Hemisphere"."$Map"_all."$LowResMesh"k_fs_LR.func.gii "$DownsampleFolder"/"$Hemisphere".atlasroi."$LowResMesh"k_fs_LR.shape.gii "$WorkingDirectory"/"$Hemisphere"."$Map"_all."$LowResMesh"k_fs_LR.func.gii
  done
  OMP_NUM_THREADS=2 wb_command -volume-to-surface-mapping "$WorkingDirectory"/goodvoxels.nii.gz "$AtlasSpaceNativeFolder"/"$Hemisphere".midthickness.native.surf.gii "$WorkingDirectory"/"$Hemisphere".goodvoxels.native.func.gii -ribbon-constrained "$AtlasSpaceNativeFolder"/"$Hemisphere".white.native.surf.gii "$AtlasSpaceNativeFolder"/"$Hemisphere".pial.native.surf.gii
  OMP_NUM_THREADS=2 wb_command -metric-mask "$WorkingDirectory"/"$Hemisphere".goodvoxels.native.func.gii "$AtlasSpaceNativeFolder"/"$Hemisphere".roi.native.shape.gii "$WorkingDirectory"/"$Hemisphere".goodvoxels.native.func.gii
  OMP_NUM_THREADS=2 wb_command -metric-resample "$WorkingDirectory"/"$Hemisphere".goodvoxels.native.func.gii "$AtlasSpaceNativeFolder"/"$Hemisphere".sphere.${RegName}.native.surf.gii "$DownsampleFolder"/"$Hemisphere".sphere."$LowResMesh"k_fs_LR.surf.gii ADAP_BARY_AREA "$WorkingDirectory"/"$Hemisphere".goodvoxels."$LowResMesh"k_fs_LR.func.gii -area-surfs "$AtlasSpaceNativeFolder"/"$Hemisphere".midthickness.native.surf.gii "$DownsampleFolder"/"$Hemisphere".midthickness."$LowResMesh"k_fs_LR.surf.gii -current-roi "$AtlasSpaceNativeFolder"/"$Hemisphere".roi.native.shape.gii
  OMP_NUM_THREADS=2 wb_command -metric-mask "$WorkingDirectory"/"$Hemisphere".goodvoxels."$LowResMesh"k_fs_LR.func.gii "$DownsampleFolder"/"$Hemisphere".atlasroi."$LowResMesh"k_fs_LR.shape.gii "$WorkingDirectory"/"$Hemisphere".goodvoxels."$LowResMesh"k_fs_LR.func.gii

  OMP_NUM_THREADS=2 wb_command -volume-to-surface-mapping "$InData" "$AtlasSpaceNativeFolder"/"$Hemisphere".midthickness.native.surf.gii "$ResultsFolderSurf"/"$Hemisphere".native.func.gii -ribbon-constrained "$AtlasSpaceNativeFolder"/"$Hemisphere".white.native.surf.gii "$AtlasSpaceNativeFolder"/"$Hemisphere".pial.native.surf.gii -volume-roi "$WorkingDirectory"/goodvoxels.nii.gz
  OMP_NUM_THREADS=2 wb_command -metric-dilate "$ResultsFolderSurf"/"$Hemisphere".native.func.gii "$AtlasSpaceNativeFolder"/"$Hemisphere".midthickness.native.surf.gii 10 "$ResultsFolderSurf"/"$Hemisphere".native.func.gii -nearest
  OMP_NUM_THREADS=2 wb_command -metric-mask  "$ResultsFolderSurf"/"$Hemisphere".native.func.gii "$AtlasSpaceNativeFolder"/"$Hemisphere".roi.native.shape.gii  "$ResultsFolderSurf"/"$Hemisphere".native.func.gii
  OMP_NUM_THREADS=2 wb_command -metric-resample "$ResultsFolderSurf"/"$Hemisphere".native.func.gii "$AtlasSpaceNativeFolder"/"$Hemisphere".sphere.${RegName}.native.surf.gii "$DownsampleFolder"/"$Hemisphere".sphere."$LowResMesh"k_fs_LR.surf.gii ADAP_BARY_AREA "$ResultsFolderSurf"/"$Hemisphere".atlasroi."$LowResMesh"k_fs_LR.func.gii -area-surfs "$AtlasSpaceNativeFolder"/"$Hemisphere".midthickness.native.surf.gii "$DownsampleFolder"/"$Hemisphere".midthickness."$LowResMesh"k_fs_LR.surf.gii -current-roi "$AtlasSpaceNativeFolder"/"$Hemisphere".roi.native.shape.gii
  OMP_NUM_THREADS=2 wb_command -metric-mask "$ResultsFolderSurf"/"$Hemisphere".atlasroi."$LowResMesh"k_fs_LR.func.gii "$DownsampleFolder"/"$Hemisphere".atlasroi."$LowResMesh"k_fs_LR.shape.gii "$ResultsFolderSurf"/"$Hemisphere".atlasroi."$LowResMesh"k_fs_LR.func.gii
done

echo -e "\n### END: Ribbon Volume To Surface Mapping"


echo -e "\n### START: Surface Smoothing"

SmoothingFWHM="$FWHM"

Sigma=`echo "$SmoothingFWHM / ( 2 * ( sqrt ( 2 * l ( 2 ) ) ) )" | bc -l`
for Hemisphere in L R ; do
#  OMP_NUM_THREADS=2 wb_command -metric-smoothing "$AtlasSpaceNativeFolder"/"$Hemisphere".midthickness.native.surf.gii "$ResultsFolderSurf"/"$Hemisphere".native.func.gii "$Sigma" "$ResultsFolderSurf"/s"$SmoothingFWHM"."$Hemisphere".native.func.gii -roi "$AtlasSpaceNativeFolder"/"$Hemisphere".roi.native.shape.gii
  OMP_NUM_THREADS=2 wb_command -metric-smoothing "$DownsampleFolder"/"$Hemisphere".midthickness."$LowResMesh"k_fs_LR.surf.gii "$ResultsFolderSurf"/"$Hemisphere".atlasroi."$LowResMesh"k_fs_LR.func.gii "$Sigma" "$ResultsFolderSurf"/s"$SmoothingFWHM".atlasroi."$Hemisphere"."$LowResMesh"k_fs_LR.func.gii -roi "$DownsampleFolder"/"$Hemisphere".atlasroi."$LowResMesh"k_fs_LR.shape.gii
done

echo -e "\n### END: Surface Smoothing"


echo -e "\n### START: Create Dense TimeSeries for cortices"

# All vertices
TR_vol=`fslval "$InData" pixdim4 | cut -d " " -f 1`
echo -e "    - Left hemisphere"
OMP_NUM_THREADS=2 wb_command -cifti-create-dense-timeseries "$ResultsFolderSurf"/func.L.dtseries.nii -left-metric "$ResultsFolderSurf"/s"$SmoothingFWHM".atlasroi.L."$LowResMesh"k_fs_LR.func.gii -roi-left "$DownsampleFolder"/L.atlasroi."$LowResMesh"k_fs_LR.shape.gii -timestep "$TR_vol"
#OMP_NUM_THREADS=2 wb_command -cifti-create-dense-timeseries "$ResultsFolderSurf"/func.L.dtseries.nii -left-metric "$ResultsFolderSurf"/s"$SmoothingFWHM".L.native.func.gii -roi-left "$AtlasSpaceNativeFolder"/L.roi.native.shape.gii -timestep "$TR_vol"
OMP_NUM_THREADS=2 wb_command -cifti-separate "$ResultsFolderSurf"/func.L.dtseries.nii COLUMN -metric CORTEX_LEFT "$ResultsFolderSurf"/func.L.func.gii
echo -e "    - Right hemisphere"
OMP_NUM_THREADS=2 wb_command -cifti-create-dense-timeseries "$ResultsFolderSurf"/func.R.dtseries.nii -right-metric "$ResultsFolderSurf"/s"$SmoothingFWHM".atlasroi.R."$LowResMesh"k_fs_LR.func.gii -roi-right "$DownsampleFolder"/R.atlasroi."$LowResMesh"k_fs_LR.shape.gii -timestep "$TR_vol"
#OMP_NUM_THREADS=2 wb_command -cifti-create-dense-timeseries "$ResultsFolderSurf"/func.R.dtseries.nii -right-metric "$ResultsFolderSurf"/s"$SmoothingFWHM".R.native.func.gii -roi-right "$AtlasSpaceNativeFolder"/R.roi.native.shape.gii -timestep "$TR_vol"
OMP_NUM_THREADS=2 wb_command -cifti-separate "$ResultsFolderSurf"/func.R.dtseries.nii COLUMN -metric CORTEX_RIGHT "$ResultsFolderSurf"/func.R.func.gii

echo -e "\n### END: Create Dense TimeSeries for cortices"


