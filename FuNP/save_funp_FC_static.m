function save_funp_FC_static(save_folder, preproc_data_total_list, atlas_path, atlas_name, atlas_ext, standard_path, standard_name, standard_ext)
    save(strcat(save_folder,'/FuNP_FC_static_settings.mat'), 'preproc_data_total_list', 'atlas_path', 'atlas_name', 'atlas_ext', 'standard_path', 'standard_name', 'standard_ext');
end