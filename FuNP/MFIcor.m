function varargout = MFIcor(varargin)
% MFICOR MATLAB code for MFIcor.fig
%      MFICOR, by itself, creates a new MFICOR or raises the existing
%      singleton*.
%
%      H = MFICOR returns the handle to a new MFICOR or the handle to
%      the existing singleton*.
%
%      MFICOR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MFICOR.M with the given input arguments.
%
%      MFICOR('Property','Value',...) creates a new MFICOR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before MFIcor_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to MFIcor_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help MFIcor

% Last Modified by GUIDE v2.5 04-May-2018 21:34:32

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MFIcor_OpeningFcn, ...
                   'gui_OutputFcn',  @MFIcor_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before MFIcor is made visible.
function MFIcor_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to MFIcor (see VARARGIN)

% Choose default command line output for MFIcor
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes MFIcor wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = MFIcor_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in YN.
function YN_Callback(hObject, eventdata, handles)
% hObject    handle to YN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns YN contents as cell array
%        contents{get(hObject,'Value')} returns selected item from YN
contents = cellstr(get(hObject,'String'));
YN = contents{get(hObject,'Value')};
handles.YN = YN;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function YN_CreateFcn(hObject, eventdata, handles)
% hObject    handle to YN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in OK.
function OK_Callback(hObject, eventdata, handles)
% hObject    handle to OK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ischar(handles.YN) == 1
    mficor = handles.YN;
    assignin('base', 'mficor', mficor);
else
    error('### You did not select any option ###')
end
if length(mficor) == 0
    error('### You did not select any option ###')
end
close(MFIcor)


% --- Executes on button press in CANCEL.
function CANCEL_Callback(hObject, eventdata, handles)
% hObject    handle to CANCEL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(MFIcor)
