# FuNP

FuNP (Fusion of Neuroimaging Preprocessing Pipeline) \
: A fully automated volume- and surface-based preprocessing pipelines for T1-weighted and fMRI data 

* **Please cite the paper if you use this code** \
*B.-y. Park, K. Byeon, and H. Park.* FuNP (Fusion of Neuroimaging Preprocessing) pipelines: A fully automated preprocessing software for functional magnetic resonance imaging. *Frontiers in Neuroinformatics* 13:5 (2019). \
https://www.frontiersin.org/articles/10.3389/fninf.2019.00005/full

* **For FuNP_FC, referred the followings:** \
*Brain connectivity toolbox:* https://sites.google.com/site/bctnet/ \
*Group ICA of fMRI Toolbox:* https://trendscenter.org/software/gift/

# Prerequisite
* **Prerequisite software download pages** \
AFNI: https://afni.nimh.nih.gov/download \
FSL: https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FslInstallation \
FIX: https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FIX \
FreeSurfer: http://freesurfer.net/fswiki/DownloadAndInstall \
Workbench: https://www.humanconnectome.org/software/get-connectome-workbench \
ANTs: https://github.com/ANTsX/ANTs

* **FuNP was developed under following conditions** \
MATLAB v.2016b \
AFNI v.18.1.33 \
FSL v.5.0.8 \
FIX v.1.065 \
FreeSurfer v.6.0.0 \
Workbench v.1.2.3

# Usage
* **Download 'FuNP' and 'SurfaceAtlas'**

* **Add the downloaded folder (SurfaceAtlas & FuNP) in MATLAB: addpath(genpath('~/FuNP'))**

* **Type 'funp' in the command window**
 
  - **Major outputs from Volume T1** \
  RPI_?: Re-oriented (orientation can be any direction: RAI, LAS,...) \
  MFIcor_?: Bias field corrected \
  SS_?: Skull removed \
  STDregis_?: Registered onto the standard space \
  seg_?: Segmented into GM, WM, and CSF

  - **Major outputs from Volume fMRI** \
  RPI_?: Re-oriented (orientation can be any direction: RAI, LAS,...) \
  delNvol_?: First N volumes are discarded \
  STC_?: Slice timing corrected \
  dc_?: EPI distortion corrected \
  MS_?: Volume scrubbed based on the frame-wise displacement \
  MC_?: Head motion corrected \
  BET_?: Skull removed \
  IntNorm_?: Intensity normalized \
  Filtered_clean_?: Nuisance variable removed \
  Func2STD_4D_?: Registered onto the standard space (4D) \
  TempFilt_?: Temporal filtered \
  Smooth_?: Spatial smoothed

  - **Major outputs from Surface T1** \
  fs_initial: Outputs from the recon-all function of FreeSurfer \
  wb_adjust: Surface adjusted files using Workbench \
  Major outputs: ~/wb_adjust/MNINonLinear/fsaverage_LR?k

  - **Major outputs from Surface fMRI** \
  Major outputs: ~/wb_adjust/MNINonLinear/Results/? \
  Volume: Same as the outputs from the volume fMRI \
  Surface: Surface mapped fMRI data \
  ?.native.func.gii: Surface mapped fMRI data \
  s?.atlasroi.?.32k_fs_LR.func.gii: Surface smoothed data \
  func.?.func.gii: Final preprocessed fMRI data

* **Type 'funp_FC' in the command window**

  - **Outputs from static connectivity analysis** \
  ConnMatR: connectivity matrix with r-values (N x N, N: number of ROIs) \
  RealNet_weiZ_?: Soft-thresholded, z-transformed connectivity matrix \
  netparam_? \
    -- Centrality measures: Hubness, Betweenness centrality (BC), Degree centrality (DC), Eigenvector centrality (EVC) (1 x N)  \
    -- Efficiency measures: Global efficiency (Eglob) (1 x 1), Local efficiency (Eloc) (1 x N) \
    -- Module measures: modularity (Q) (1 x 1), Module index (Ci), Within-module degree z-score (WMDZ), Participation coefficient (PC) (1 x N)
  
  - **Outputs from dynamic connectivity analysis**
  OrigMat: Original dynamic connectivity matrices (N x N x W, W: number of sliding windows) \
  RegMat: L1 regularized dynamic connectivity matrices using graphical LASSO (N x N x W) \
  best_lambda: lambda value for regularization \
  netparam \
    -- Centrality measures: Hubness, Betweenness centrality (BC), Degree centrality (DC), Eigenvector centrality (EVC) (W x N) \
    -- Efficiency measures: Global efficiency (Eglob) (W x 1), Local efficiency (Eloc) (W x N) \
    -- Module measures: modularity (Q) (W x 1), Module index (Ci), Within-module degree z-score (WMDZ), Participation coefficient (PC) (W x N)

* **To run many data at one time, I recommend the users to save the options and run in MATLAB using for loop**

# Websites

* **LAB (CAMIN: Computational Analysis for Multimodal Integrative Neuroimaging):** https://www.caminlab.com/
* **LAB (MIPL: Medical Image Processing Lab):** https://mipskku.wixsite.com/mipl